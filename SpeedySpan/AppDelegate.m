//
//  AppDelegate.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 25/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "SPImageFullScreenViewController.h"

@implementation AppDelegate
@synthesize HUD;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
      [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navController =[[UINavigationController alloc]init];
    //Get current location
    self.locationManager  = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    [self.locationManager startUpdatingLocation];
    
       return YES;
}
#pragma mark -CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    if(oldLocation != newLocation)
    {
        self.userLocation=newLocation.coordinate;
        
    }
}
#pragma mark -PUSHNOTIFICATION
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken
{
	[[NSUserDefaults standardUserDefaults] setObject:devToken forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"did receive notificaton");
    NSString *message;
    NSString *notificationType;
    NSString *strValue = GETVALUE(@"userid");
    self.dicNotificationInfo = [[NSMutableDictionary alloc]initWithDictionary:userInfo];
    if(strValue==(id) [NSNull null] || [strValue length]==0 || [strValue isEqualToString:@""]|| [strValue isEqualToString:@"(null)"])
    {
       
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        ViewController *objLogin =[storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
        [self.navController pushViewController:objLogin animated:YES];
        
    }else
    {
        UIApplicationState state = [application applicationState];
        if (state == UIApplicationStateInactive || state == UIApplicationStateBackground)
        {
            message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
            notificationType=[[userInfo valueForKey:@"aps"] valueForKey:@"type"];
            SPImageFullScreenViewController *objFullImage = [[SPImageFullScreenViewController alloc]initWithNibName:@"SPImageFullScreenViewController" bundle:nil];
            objFullImage.isFromPush = YES;
            objFullImage.circleID=[[userInfo valueForKey:@"aps"] valueForKey:@"circleid"];
            objFullImage.circleName=[[userInfo valueForKey:@"aps"] valueForKey:@"circlename"];
            UINavigationController *controller = (UINavigationController*)self.window.rootViewController;
            
            [controller pushViewController:objFullImage animated:YES];
            
        }else{
            message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Open", nil];
            [alert show];
        }
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex==0) {
        
    }else{
        
        SPImageFullScreenViewController *objFullImage = [[SPImageFullScreenViewController alloc]initWithNibName:@"SPImageFullScreenViewController" bundle:nil];
        objFullImage.isFromPush = YES;
        objFullImage.circleID=[[self.dicNotificationInfo valueForKey:@"aps"] valueForKey:@"circleid"];
        objFullImage.circleName=[[self.dicNotificationInfo valueForKey:@"aps"] valueForKey:@"circlename"];
        UINavigationController *controller = (UINavigationController*)self.window.rootViewController;
        
        [controller pushViewController:objFullImage animated:YES];

    }
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [FBAppCall handleDidBecomeActiveWithSession:self.session];
    //Push notification
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationType)(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound)];
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma mark Facebook
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBSession.activeSession handleOpenURL:url];
}
#pragma mark Facebook Logout
-(void)FacebookLogOut
{
    NSLog(@"Logged out of facebook");
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    { //Delete cookie
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
    //Clear session
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession.activeSession close];
    [FBSession setActiveSession:nil];
}

@end
