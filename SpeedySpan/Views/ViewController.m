//
//  ViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 25/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "ViewController.h"
#import "SPCreateCircleViewController.h"
#import "SPCircleDetailsViewController.h"
#import "CircleLayout.h"
#import "AFNetworking.h"
#import "VendorID.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Hide navigation bar
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    //Font for fb buttons
    self.btnFacebook.titleLabel.font=OPENSANS_LIGHT(16);
    int userId = [GETVALUE(@"userid") intValue];
    if (userId!=0) {
       [self performSegueWithIdentifier:@"firstViewToHomeView" sender:self];
    }
    
    
}
#pragma mark UIButton Click

-(IBAction)loginWithFBBtnClk:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
   [appDelegate FacebookLogOut]; //Clear session
    //Show hud
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    //Fetch facebook user information with permission
    [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_location",@"user_birthday",@"user_hometown"] allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error)
        {
            switch (state) {
                case FBSessionStateOpen:
                {
                    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
                    {
                        if (error) {
                            //If Off from facebook setting
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Facebook" message:@"To use your facebook account with this app,open Setting -> Facebook and make sure this app is turned on." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        [HUDCommon killHUD];
                                                      
                                                      
                        }
                            else
                            {
                                //Get user details from facebook
                                
                                NSString *fbAccessToken = [FBSession activeSession].accessTokenData.accessToken; //Get accesstoken
                                
                                UPDATEVALUE(@"fbAccessToken", fbAccessToken);
                                //Large picture url
                                //NSString *urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",result.id];
                                //Profile picture url
                                NSString *urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture",result.id];
                                 UPDATEVALUE(@"socialid", result.id);
                                //Taking value in NSMutableDictionary
                                NSMutableDictionary *dicResult=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[result objectForKey:@"name"],@"username",[result objectForKey:@"email"],@"email",[result objectForKey:@"gender"],@"gender",[result objectForKey:@"id"],@"password",[result objectForKey:@"id"],@"socialid",urlString,@"userprofileimage", nil];
                               
                                [self loginWithFBCredential:dicResult];
                               
                                                      
                            }
                    }];
                }
                break;
                                              
                case FBSessionStateClosed:
                {
                [HUDCommon killHUD];
                }
                break;
                case FBSessionStateClosedLoginFailed:
                {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Facebook" message:@"To use your facebook account with this app,open Setting -> Facebook and make sure this app is turned on." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                 [HUDCommon killHUD];
                 [FBSession.activeSession closeAndClearTokenInformation];
                }
                break;
                                              
                default:
                 break;
            }
                                      
        } ];
    
}
-(void)loginWithFBCredential:(NSMutableDictionary *)dicValue
{
   
    
	// Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kREGISTER];
    // NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kGETLOGIN];
    //Send url to server
    [SPNetworkRequest loginWithFBDeatiilsWithUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
           NSString *strResponse =[objParse parseRegistrationResponse:response];
            //Push create circle view
            if ([strResponse isEqualToString:@"yes"]) {
                /*if ([GETVALUE(@"circlecount") intValue]!=0) {
                  [self performSegueWithIdentifier:@"firstViewToHomeView" sender:self];
                }else{
                 [self performSegueWithIdentifier:@"createYourCircle" sender:self];
                }*/
                [self submitRemoteNotificationsWithDeviceToken];
            }else{
                 [HUDCommon killHUD];
                 //[self performSegueWithIdentifier:@"createYourCircle" sender:self];
                [HUDCommon showMessage:strResponse withColor:kRED showOnTOP:YES];
            }
           
            
          
        });
        
        
    }];
    
    
}
-(void) submitRemoteNotificationsWithDeviceToken
{
    //Send url to server
    [SPNetworkRequest registerForPushWithUrl:@"" withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"push response = %@",response);
            
            [HUDCommon killHUD];
            if ([GETVALUE(@"circlecount") intValue]!=0) {
                [self performSegueWithIdentifier:@"firstViewToHomeView" sender:self];
            }else{
                [self performSegueWithIdentifier:@"createYourCircle" sender:self];
            }
                
                
            
            
        });
        
        
    }];
	
	
}

#pragma mark Seguue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"createYourCircle"]) {
       
        
        
    }
    
    
}
-(IBAction)loginWithFBBtnClk1:(id)sender
{/*
    SPCircleDetailsViewController *obj = [[SPCircleDetailsViewController alloc] initWithCollectionViewLayout:[[CircleLayout alloc] init]];
    [self.navigationController pushViewController:obj animated:YES];
  */
}
#pragma mark Facebook Logout
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
