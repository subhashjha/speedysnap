//
//  SPCreateCircleViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPCreateCircleViewController : UIViewController

@property(nonatomic,strong)IBOutlet UILabel *lblTitle;
@property(nonatomic,strong)IBOutlet UIButton *btnCreateCircle;
-(IBAction)btnHelpClk:(id)sender;
-(IBAction)btnCreateCircleClk:(id)sender;
@end
