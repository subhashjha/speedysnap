//
//  SPSettingsViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPSettingsViewController.h"

@interface SPSettingsViewController ()

@end

@implementation SPSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.lblTitle.font=OPENSANS(22);
    self.lblRateApp.font=OPENSANS(22);
    self.lblNotification.font=OPENSANS(22);
    self.btnlogout.titleLabel.font=OPENSANS_LIGHT(18);
}
#pragma mark - UIButton Action

//Rate bitton click
-(IBAction)btnRateAppClk:(id)sender
{
    NSURL *url = [NSURL URLWithString:@"https://itunesconnect.apple.com/WebObjects/iTunesConnect.woa/wo/33.0.0.7.3.0.9.3.3.1.0.13.3.1.1.11.9.0.1.5"];
    
    if (![[UIApplication sharedApplication] openURL:url])
        NSLog(@"%@%@",@"Failed to open url:",[url description]);


}
//Notfication Button click
-(IBAction)btnNotificationClk:(id)sender
{
    BOOL ison = GETBOOLVALUE(@"yes");//condition for on or off
    if (ison) {
        selected=YES;
    }else{
        selected=NO;
    }
    if (!selected )
    {   selected = YES;
        
       // [self removeDeviceFromNotification];
        UPDATEBOOLVALUE(@"yes", YES);//Change Bool value
       //Change Button Image
        [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
        // Do any additional setup after loading the view.
       
        NSString *url=[NSString stringWithFormat:@"%@unregisteredDevice?userid=%@",BASE_URL,GETVALUE(@"userid")];
        //Send url to server
        [SPNetworkRequest sendInvitation:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
            // Back to main thread
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                NSLog(@"response = %@",response);
                               //Push create circle view
                [HUDCommon killHUD];
                
                 [self.btnNotification setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
                
                
            });
            
            
        }];

    }
    else{
        
        selected = NO;
        UPDATEBOOLVALUE(@"yes", NO);
         [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
      //  [self registerForPushNotification];
        //Change Button Image
        [SPNetworkRequest registerForPushWithUrl:@"" withParameter:nil completionHandler:^(NSString *response, NSError *error){
            // Back to main thread
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                NSLog(@"push response = %@",response);
                
                [HUDCommon killHUD];
                
                [self.btnNotification setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
                
                
            });
            
            
        }];

        
    }
}
-(IBAction)btnLogoutClk:(id)sender
{ //Logout from App
    REMOVEVALUE(@"userid");
    REMOVEVALUE(@"circleid");
    REMOVEVALUE(@"fbAccessToken");
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate FacebookLogOut]; //Clear FBsession
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(IBAction)btnBackClk:(id)sender
{
    //Back click
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
