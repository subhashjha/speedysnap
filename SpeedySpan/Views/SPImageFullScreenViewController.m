//
//  SPImageFullScreenViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPImageFullScreenViewController.h"
#import "SPCircleImageData.h"
#import "SPCircleDetailsViewController.h"
#import "CircleLayout.h"
#import "SPCircleListData.h"
@interface SPImageFullScreenViewController ()

@end

@implementation SPImageFullScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
      self.lblTitle.font=OPENSANS(22);
    if (self.isFromPush) {
        [self fetchCircleWithId:self.circleID andUserId:GETVALUE(@"userid")];
    }else{
    self.lblTitle.text=self.circleName;
    //configure carousel
    self.carousel.type = iCarouselTypeLinear;
    [self.carousel reloadData];
    }
    

}
-(IBAction)btnBackClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - API CALL FetchCircle
-(void)fetchCircleWithId:(NSString *)strCircleId andUserId:(NSString *)strUserID
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserID,@"userid",strCircleId,@"circleid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:KFETCHCIRCLEWITHID];
    //Send url to server
    [SPNetworkRequest getFriendListWithUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            self.arrImageList = [[NSMutableArray alloc]init];
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            //Get circle response
            NSMutableArray *arrList =[objParse parseCircleListWithCircleIDResponse:response];
            if ([arrList count]>0) {
                SPCircleListData *data = [arrList objectAtIndex:0];
                //Get admin id value
                self.adminId=[SafeString(data.strCircleAdminID) intValue];
                if ([data.arrCircleImage count]==0) {
                   
                }else{
                    self.arrImageList=data.arrCircleImage;
                    //create carousel
                    self.lblTitle.text=data.strCircleName;
                    //configure carousel
                    self.carousel.type = iCarouselTypeLinear;
                    [self.carousel reloadData];
                }
            }
            [HUDCommon killHUD];
            
            
            
            
        });
        
        
    }];
    
}

#pragma mark -
#pragma mark iCarousel methods
#pragma mark -
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    NSLog(@" arr count      %lu",(unsigned long)[self.arrImageList count]);
    return [self.arrImageList count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    SPCircleImageData *data = [self.arrImageList objectAtIndex:index];
    //create new view if no view is available for recycling
    UILabel *label = nil;
    if (view == nil)
    {
        if ((SafeString(data.strImageUrl)).length>2) {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320.0f, 200.0f)];
            label = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 280.0f, 22.0f)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentLeft;
            label.textColor=[UIColor whiteColor];
            label.font = OPENSANS(22);
            [view addSubview:label];
            //Download and set image for UIImageView
            [((UIImageView *)view) setImageURL:[NSURL URLWithString:SafeString(data.strImageUrl)]];
            data.image=((UIImageView *)view).image;
            data.imageView=((UIImageView *)view);
       }
    }
    else {
        if ((SafeString(data.strImageUrl)).length>2) {
            //Download and set image for UIImageView
            [((UIImageView *)view) setImageURL:[NSURL URLWithString:SafeString(data.strImageUrl)]];
             data.image=((UIImageView *)view).image;
              data.imageView=((UIImageView *)view);
        }
    }
    label.text=SafeString(data.strUploaderName);
    
    return view;
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    // SPCircleImageData *data = [self.arrImageList objectAtIndex:index];
    self.selectedIndex=index;
    [self.navigationController.view addSubview:self.customUIActionSheetViewController.view];
    self.customUIActionSheetViewController.delegate=self;
    [self.customUIActionSheetViewController viewWillAppear:YES];
    
    
}
-(void)selectedButton:(int)tag
{
    if (tag==10) {
        SPCircleImageData *data = [self.arrImageList objectAtIndex: self.selectedIndex];
        if (self.isAdmin) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Do you want to delete this image?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            [alert show];
        }else{
            int userId = [GETVALUE(@"userid") intValue];
            //int userId = 2;
            if ([data.strUploadedBy intValue]== userId) {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Do you want to delete this image?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                [alert show];
            }else{
                [HUDCommon showMessage:@"You can not delete this image!" withColor:kRED showOnTOP:YES];
                
            }
        }
    }
    if (tag==11) {
        
       //Save image to gallery
        SPCircleImageData *data = [self.arrImageList objectAtIndex: self.selectedIndex];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:data.strImageUrl]]];
         UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);

    }else{
        
    }
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
    }else{
        
        SPCircleImageData *data = [self.arrImageList objectAtIndex:self.selectedIndex];
        [self deleteImageWithId:data.strImageId];
         
        
    }
}
-(void)deleteImageWithId:(NSString *)strImageId
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view
    NSString *url=[NSString stringWithFormat:@"%@deleteImage?imageid=%@",BASE_URL,strImageId];
    //Send url to server
    [SPNetworkRequest deleteFromCircleUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            //Get parse response
            NSString *strResponse =[objParse parseResponseString:response];
            [HUDCommon killHUD];
            if ([strResponse isEqualToString:@"yes"]) {
                AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelgObj.isDeleteImage=YES;
                [self.arrImageList removeObjectAtIndex:self.selectedIndex];
                [self.carousel reloadData];
            }else
            {
                [HUDCommon showMessage:@"Image not Deleted. Please try again later." withColor:kRED showOnTOP:YES];
            }
            [HUDCommon killHUD];
        });
        
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
