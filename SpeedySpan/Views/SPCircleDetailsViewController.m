//
//  SPCircleDetailsViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPCircleDetailsViewController.h"
#import "Cell.h"
#import "SPCircleListData.h"
#import "SPFriendsDetailsData.h"
#import "SPCircleImageData.h"
#import "SPImageFullScreenViewController.h"
#import "SPSelectMemberViewController.h"

@interface SPCircleDetailsViewController ()

@end

@implementation SPCircleDetailsViewController



- (void)viewDidLoad
{
    
    //self.strCircleId=@"1";
  
    UPDATEVALUE(@"circleid", self.strCircleId);
     float systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    UIView *viewContainer = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,580)];
      //Add background image on uiview
    UIImageView *imageview ;
    if (IS_IPHONE_5) {
    if (systemVersion<=6.1) {
        imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,580)];
    }else{
    imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,580)];
    }
    }else
    {
        if (systemVersion<=6.1) {
            imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,480)];
        }else{
            imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,320,480)];
        }

 
    }
    imageview.image=[UIImage imageNamed:@"backgroundwithline.png"];
    [viewContainer addSubview:imageview];
    
    [self.view addSubview:viewContainer];//Add viewContainer on main view
    
    [viewContainer addSubview:self.collectionView]; // Add collection view on viewContainer
    //Drow circle on viewContainer
   
    if (IS_IPHONE_5) {
      if (systemVersion<=6.1) {
            circleView = [[UIView alloc] initWithFrame:CGRectMake(30,110,260,260)];
      }else{
         circleView = [[UIView alloc] initWithFrame:CGRectMake(30,115,260,260)];
      }
       
    }else{
       
        if (systemVersion<=6.1) {
              circleView = [[UIView alloc] initWithFrame:CGRectMake(30,95,260,260)];
        }else{
        circleView = [[UIView alloc] initWithFrame:CGRectMake(30,110,260,260)];
        }
      
    }
    circleView.alpha = 0.7;
    circleView.layer.cornerRadius = 130;//Radius for circle
    
    circleView.layer.borderColor = [UIColor whiteColor].CGColor;//Cricle border colr
    circleView.layer.borderWidth = 2.0f; //Circle width
    circleView.backgroundColor = [UIColor clearColor]; //Circle background color
    [viewContainer addSubview:circleView];//Add circle on viewContainer
    [circleView sendSubviewToBack:self.collectionView]; //Send back to circle view
    [circleView sendSubviewToBack:self.view];
    [self.collectionView bringSubviewToFront:circleView];
    
   // circleView.hidden=YES;
    //Add home button on navBar
    UIButton *btnHome = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnHome addTarget:self
               action:@selector(btnHomeClk:)
     forControlEvents:UIControlEventTouchUpInside];//Button action
    [btnHome setBackgroundImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];//Set image for button
    [btnHome setTitle:@"" forState:UIControlStateNormal];
    //Frame for button position
    if (IS_IPHONE_5) {
        btnHome.frame = CGRectMake(8.0, 7.0, 33.0, 33.0);
    }else{
    btnHome.frame = CGRectMake(8.0, 14.0, 33.0, 33.0);
    }
    [self.collectionView addSubview:btnHome];//Add home button on clollection view
    //Take photo button
    UIButton *btnAddPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnAddPhoto addTarget:self
                action:@selector(addPhotoClk:)
      forControlEvents:UIControlEventTouchUpInside];
    [btnAddPhoto setBackgroundImage:[UIImage imageNamed:@"addPhoto.png"] forState:UIControlStateNormal];
    btnAddPhoto.titleLabel.font=OPENSANS_LIGHT(18);
    [btnAddPhoto setTitle:@"Add Photo" forState:UIControlStateNormal];
    //Frame for take photo button
    if (IS_IPHONE_5) {
       btnAddPhoto.frame = CGRectMake(0.0, 440.0, 320.0, 28.0);
    }else{
     btnAddPhoto.frame = CGRectMake(0.0, 410.0, 320.0, 28.0);
    }
  
    [self.collectionView addSubview:btnAddPhoto];//Add uibutton On collection view
    //Add title on navBar
    
 if (IS_IPHONE_5) {
   navTitle = [[UILabel alloc] initWithFrame:CGRectMake(55, 10, 220, 25)];
 }else{
     navTitle = [[UILabel alloc] initWithFrame:CGRectMake(55, 15, 220, 25)];
 }
    
    [navTitle setTextColor:[UIColor whiteColor]];
    [navTitle setBackgroundColor:[UIColor clearColor]];
    navTitle.font=OPENSANS(22);
    navTitle.textAlignment=NSTextAlignmentCenter;
    navTitle.text=self.strCircleName;
    [self.collectionView addSubview:navTitle];
    
    [viewContainer bringSubviewToFront:self.collectionView];
   //UITapGestureRecognizer to remove from circle
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.collectionView addGestureRecognizer:tapRecognizer];
    [self.collectionView registerClass:[Cell class] forCellWithReuseIdentifier:@"MY_CELL"];
    self.collectionView.backgroundColor=[UIColor clearColor];//Set collection view background
  //  self.collectionView.frame=CGRectMake(0, 0, 320, 480);
   // self.collectionView.contentSize=CGSizeMake(320, 300);
    //self.collectionView.pagingEnabled = YES;
    self.collectionView.scrollEnabled  =NO;
    [self.collectionView reloadData];//Reload collection view
    
    
     [self fetchCircleWithId:self.strCircleId andUserId:GETVALUE(@"userid")];
   
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //FetchCircleWithID
    AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelgObj.isDeleteImage) {
         appDelgObj.isDeleteImage=NO;
        [self fetchCircleWithId:self.strCircleId andUserId:GETVALUE(@"userid")];
       
    }
}
//Button home
-(void)btnHomeClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
//Button home
-(void)btnSaveClk:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    SPSelectMemberViewController *objSelectMember =  [storyboard instantiateViewControllerWithIdentifier:@"selectMemberController"];
    objSelectMember.fromDetailsView=YES;
    [self.navigationController pushViewController:objSelectMember animated:YES];
   
}
//Button ad photo click
-(void)addPhotoClk:(id)sender
{
    self.customUIActionSheetViewController=[[CustomUIActionSheetViewController alloc]init];
    [self.collectionView addSubview:self.customUIActionSheetViewController.view];
    self.customUIActionSheetViewController.delegate=self;
    [self.customUIActionSheetViewController viewWillAppear:NO];
    
}
-(void)selectedButton:(int)tag
{
    if (tag==1) {
        [self openPictureLibrary:NO];
    }
    if (tag==2) {
        [self openPictureLibrary:YES];
    }else{
        
    }
}
-(void)cancelButtonClicked:(id)sender
{
    [self.actionSheet dismissWithClickedButtonIndex:2 animated:YES];
}
-(void)cameraButtonClicked:(id)sender
{
    [self openPictureLibrary:YES];
    [self.actionSheet dismissWithClickedButtonIndex:1 animated:YES];
    
    
}
-(void)galleryButtonClicked:(id)sender
{
    
    [self openPictureLibrary:NO];
    [self.actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    
}
-(void)openPictureLibrary:(BOOL)isCamera
{
    if (isCamera) {
        if([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]){
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:nil];
        }else{
            [HUDCommon showMessage:@"Camera is not available." withColor:kRED showOnTOP:YES];
            
        }
        
    }else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage   *smallImage = [self imageWithImage:image scaledToSize:CGSizeMake(image.size.width/2, image.size.height/2)];
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(smallImage)];
    [self uploadImageWithIMageData:imageData andCircleId:self.strCircleId andUserId:GETVALUE(@"userid")];
}
- (UIImage*)imageWithImage:(UIImage*)cemraimage
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [cemraimage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
#pragma mark - API CALL FetchCircle
-(void)fetchCircleWithId:(NSString *)strCircleId andUserId:(NSString *)strUserID
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserID,@"userid",strCircleId,@"circleid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:KFETCHCIRCLEWITHID];
    //Send url to server
    [SPNetworkRequest getFriendListWithUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            self.collectionView.contentSize=CGSizeMake(320, 300);
            self.arrCircleList = [[NSMutableArray alloc]init];
            self.arrImageList = [[NSMutableArray alloc]init];
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            //Get circle response
            NSMutableArray *arrList =[objParse parseCircleListWithCircleIDResponse:response];
            if ([arrList count]>0) {
                SPCircleListData *data = [arrList objectAtIndex:0];
                //Get admin id value
                self.adminId=[SafeString(data.strCircleAdminID) intValue];
                self.arrCircleList=data.arrFriendsList;
                if (self.arrCircleList.count>0) {
                    circleView.hidden=NO;
                }
                if (self.arrCircleList.count<=10) {
                    if (self.isAdmin) {
                        //Add home button on navBar
                        UIButton *btnSave = [UIButton buttonWithType:UIButtonTypeCustom];
                        [btnSave addTarget:self
                                    action:@selector(btnSaveClk:)
                          forControlEvents:UIControlEventTouchUpInside];//Button action
                        [btnSave setBackgroundImage:[UIImage imageNamed:@"AddFrnd.png"] forState:UIControlStateNormal];//Set image for button
                        [btnSave setTitle:@"" forState:UIControlStateNormal];
                        //Frame for button position
                        if (IS_IPHONE_5) {
                            btnSave.frame = CGRectMake(280.0, 7.0, 33.0, 33.0);
                        }else{
                            btnSave.frame = CGRectMake(280.0, 14.0, 33.0, 33.0);
                        }
                        [self.collectionView addSubview:btnSave];//Add home button on clollection view
                    }else{
                        
                    }

                }
                if ([data.arrCircleImage count]==0) {
                    if (IS_IPHONE_5) {
                        self.lblNoPhoto = [[UILabel alloc] initWithFrame:CGRectMake(90, 175, 150, 100)];
                    }else{
                        self.lblNoPhoto = [[UILabel alloc] initWithFrame:CGRectMake(90, 175, 150, 100)];
                    }
                    
                    [self.lblNoPhoto setTextColor:[UIColor whiteColor]];
                    [self.lblNoPhoto setBackgroundColor:[UIColor clearColor]];
                    self.lblNoPhoto.font=OPENSANS_LIGHT(20);
                    self.lblNoPhoto.numberOfLines=3;
                    self.lblNoPhoto.textAlignment=NSTextAlignmentCenter;
                    self.lblNoPhoto.text=@"No Photos have been added to the circle.";
                    [self.collectionView addSubview:self.lblNoPhoto];
                }else{
                    self.lblNoPhoto.hidden=YES;
                    SPCircleListData *data = [arrList objectAtIndex:0];
                    self.arrImageList=data.arrCircleImage;
                    //create carousel
                    _carousel = [[iCarousel alloc] initWithFrame:CGRectMake(120, 185,100, 72)];
                    //  _carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                    _carousel.type = iCarouselTypeRotary;
                    _carousel.delegate = self;
                    _carousel.dataSource = self;
                    //add carousel to view
                    [self.collectionView addSubview:_carousel];
                }
                // NSLog(@"arr circle list = %@",self.arrCircleList);
                //navTitle.text=data.strCircleName;
               // self.strCircleName=data.strCircleName;
                [self.collectionView reloadData];//Reload collection view
            }else{
                [HUDCommon showMessage:@"Circle list is empty" withColor:kRED showOnTOP:YES];
            }
            //Push create circle view
            [HUDCommon killHUD];
            
            
            
            
        });
        
        
    }];
    
}
-(void)deleteHoleCircle:(NSString *)strCircleId
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strCircleId,@"circleid",GETVALUE(@"userid"),@"userid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kDELETECIRCLE];
    //Send url to server
    [SPNetworkRequest deleteCircleUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            //Get parse response
            NSString *strResponse =[objParse parseResponseString:response];
            [HUDCommon killHUD];
            if ([strResponse isEqualToString:@"yes"]) {
               [HUDCommon showMessage:@"Circle deleted." withColor:kGREEN showOnTOP:YES];
                [self.navigationController popViewControllerAnimated:YES];
            }else
            {
                [HUDCommon showMessage:@"Circle not Deleted. Please try again later." withColor:kRED showOnTOP:YES];
            }
            [HUDCommon killHUD];
        });
        
        
    }];
 
}
#pragma mark - API CALL Delete  Circle
-(void)deleteFriendWithId:(NSString *)strFriendId andCircleID:(NSString *)strcricleId
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strFriendId,@"friendid",strcricleId,@"circleid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:KDELETEFROMCIRCLE];
    //Send url to server
    [SPNetworkRequest deleteFromCircleUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            //Get parse response
            NSString *strResponse =[objParse parseResponseString:response];
            [HUDCommon killHUD];
            if ([strResponse isEqualToString:@"yes"]) {
                
                if (self.fromCircle) {
                    self.fromCircle=NO;
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                [HUDCommon showMessage:@"User deleted from circle." withColor:kGREEN showOnTOP:YES];
                [self.arrCircleList removeObjectAtIndex: self.tappedCellPath.row];
                if ([self.arrCircleList count]==1) {
                    //circleView.hidden=YES;
                }else{
                    circleView.hidden=NO;
                }
                [self.collectionView performBatchUpdates:^{
                    [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject: self.tappedCellPath]];
                    
                } completion:nil];
                }
            }else
            {
                [HUDCommon showMessage:@"User not Deleted. Please try again later." withColor:kRED showOnTOP:YES];
            }
            //[HUDCommon killHUD];
        });
        
        
    }];
    
}

#pragma mark - API CALL For uploading image
-(void)uploadImageWithIMageData:(NSData *)strImagedata andCircleId:(NSString *)strCircleID andUserId:(NSString *)userId
{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    NSString *strLatitude = [NSString stringWithFormat:@"%f",appDelegate.userLocation.latitude];
    NSString *strLongitude = [NSString stringWithFormat:@"%f",appDelegate.userLocation.latitude];
    NSMutableDictionary *dicParameter = [[NSMutableDictionary alloc] init];
    [dicParameter setObject:userId forKey:@"userid"];
    [dicParameter setObject:strCircleID forKey:@"circleid"];
    [dicParameter setObject:strLatitude forKey:@"latitude"];
    [dicParameter setObject:strLongitude forKey:@"longitude"];
    
    //Send url to server
    
    [SPNetworkRequest uploadImageForCircleWithParameter:dicParameter withImageData:strImagedata completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            
            NSString *strResponse =[objParse parseResponseString:response];
            [HUDCommon killHUD];
            if ([strResponse isEqualToString:@"yes"]) {
                //[HUDCommon showMessage:@"Image uploaded successfully." withColor:kGREEN showOnTOP:YES];
                //FetchCircleWithID
                [self fetchCircleWithId:self.strCircleId andUserId:GETVALUE(@"userid")];
            }else{
                [HUDCommon showMessage:@"Image not uploaded." withColor:kRED showOnTOP:YES];
            }
            
            //Push create circle view
            
            
            
            
            
        });
        
        
    }];
    
}
#pragma mark -
#pragma mark - UICollectionViewDelegate & UICollectionViewDataSource
#pragma mark -
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    return  [self.arrCircleList count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    Cell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    SPFriendsDetailsData *data = [self.arrCircleList objectAtIndex:indexPath.row];
    //Add imageview on celll
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 5, 44,44)];
    // imgView.image=[UIImage imageNamed:@"images.jpeg"];
    [imgView setImageURL:[NSURL URLWithString:(SafeString(data.strFriendImage))]];
    CALayer *imageLayer = imgView.layer;
    [imageLayer setCornerRadius:22];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    //Add image border on cell
    UIImageView *imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 55, 55)];
    imgView1.image=[UIImage imageNamed:@"circle.png"];
    
    
    [cell addSubview:imgView1];
    [cell addSubview:imgView];
    //}
    [self.collectionView bringSubviewToFront:imgView];
    
    return cell;
}




#pragma mark -UIGestureRecognizerDelegate

- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        int userid = [GETVALUE(@"userid") intValue] ;
       // int userid =2;
        if (userid ==self.adminId) {
            CGPoint initialPinchPoint = [sender locationInView:self.collectionView];
            self.tappedCellPath = [self.collectionView indexPathForItemAtPoint:initialPinchPoint];
            
            
            if ( self.tappedCellPath!=nil)
            {
                SPFriendsDetailsData *data = [self.arrCircleList objectAtIndex:self.tappedCellPath.row];
                if([data.strFriendID intValue]==userid)
                {
                     self.delCircle=YES;
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Do you want to remove this circle?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"Yes", nil];
                    [alert show];
                    
                }else{
                self.delCircle=NO;
                self.fromCircle=NO;
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Do you want to remove this friend from this circle?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"Yes", nil];
                [alert show];
                }
            }
            else
            {
                
            }
            }else{
            CGPoint initialPinchPoint = [sender locationInView:self.collectionView];
            self.tappedCellPath = [self.collectionView indexPathForItemAtPoint:initialPinchPoint];
            if (self.tappedCellPath!=nil)
            {
                 SPFriendsDetailsData *data = [self.arrCircleList objectAtIndex:self.tappedCellPath.row];
                if([data.strFriendID intValue]==userid)
                {
                    self.delCircle=NO;
                    self.fromCircle=YES;
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Delete" message:@"Do you want to remove yourself from this circle?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"Yes", nil];
                    [alert show];
                    
                }else{
                    [HUDCommon showMessage:@"You can not delete this friend!" withColor:kRED showOnTOP:YES];
                }
                
            }
        }
    }
    
}


#pragma mark -
#pragma mark iCarousel methods
#pragma mark -
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.arrImageList count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    // UILabel *label = nil;
    SPCircleImageData *data = [self.arrImageList objectAtIndex:index];
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //If imageurl is nill
        if ((SafeString(data.strImageUrl)).length>2) {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 110.0f, 72.0f)];
            //Download and set image for UIImageView
            [((UIImageView *)view) setImageURL:[NSURL URLWithString:SafeString(data.strImageUrl)]];
        }
        
        
    }
    
    return view;
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    
   SPImageFullScreenViewController *objFullImage = [[SPImageFullScreenViewController alloc]initWithNibName:@"SPImageFullScreenViewController" bundle:nil];
    objFullImage.arrImageList = [[NSMutableArray alloc]initWithArray:self.arrImageList];
    objFullImage.circleName=self.strCircleName;
    [self.navigationController pushViewController:objFullImage animated:YES];
    
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
            
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            if ([self.arrImageList count]>10) {
                return value * 0.07f;
            }else{
                if ([self.arrImageList count]>7) {
                    return value * 0.18f;
                }else{
                    
                    return value * 0.3f;
                }
            }
        }
        case iCarouselOptionFadeMax:
        {
            if (carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}
#pragma mark -
#pragma mark AlertViewDelegate
#pragma mark -
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        
    }else{
        SPFriendsDetailsData *data = [self.arrCircleList objectAtIndex:self.tappedCellPath.row];
        if (self.delCircle) {
            [self deleteHoleCircle:self.strCircleId];
        }else{
         [self deleteFriendWithId:SafeString(data.strFriendID) andCircleID:self.strCircleId];
        }
        
        
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
