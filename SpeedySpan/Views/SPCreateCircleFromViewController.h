//
//  SPCreateCircleFromViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPCreateCircleFromViewController : UIViewController<UITextFieldDelegate>

@property(nonatomic,strong)IBOutlet UILabel *lblTitle;
@property(nonatomic,strong)IBOutlet UIButton *btnSelectMenber;
@property(nonatomic,strong)IBOutlet UITextField *txtFieldCircle;
@property(nonatomic,strong)IBOutlet UILabel *lblCircleName;
@property(nonatomic,strong)IBOutlet UIScrollView *scrollView;

-(IBAction)btnSelectMemeberClk:(id)sender;
-(IBAction)btnBackClk:(id)sender;
-(IBAction)btnOutSideClk:(id)sender;

@end
