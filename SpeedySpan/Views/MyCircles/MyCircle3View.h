//
//  MyCircle3View.h
//  SpeedySpan
//
//  Created by Shivam Kumar on 3/4/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPHomeViewButton.h"
@interface MyCircle3View : UIView

@property (weak, nonatomic) IBOutlet SPHomeViewButton *btnCircleName;
@property (weak, nonatomic) IBOutlet UIImageView *img1Pic;
@property (weak, nonatomic) IBOutlet UIImageView *img2Pic;
@property (weak, nonatomic) IBOutlet UIImageView *img3Pic;

@end
