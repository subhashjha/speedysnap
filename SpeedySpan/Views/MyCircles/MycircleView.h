//
//  MycircleView.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPHomeViewButton.h"

@interface MycircleView : UIView
@property (weak, nonatomic) IBOutlet SPHomeViewButton *btnCircleName;
@end
