//
//  MyCircle1View.h
//  SpeedySpan
//
//  Created by Shivam Kumar on 3/4/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPHomeViewButton.h"
@interface MyCircle1View : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgPic;
@property (weak, nonatomic) IBOutlet SPHomeViewButton *btnCircleName;


@end
