//
//  SPHomeViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 03/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPHomeViewController.h"
#import "SPHomeViewCell.h"
#import "MyCircle1View.h"
#import "MyCircle2View.h"
#import "MyCircle3View.h"
#import "MyCircle4View.h"
#import "MycircleView.h"
#import "SPCircleListData.h"
#import "SPFriendsDetailsData.h"
#import "SPHomeViewButton.h"
#import "SPCircleDetailsViewController.h"
#import "CircleLayout.h"
@interface SPHomeViewController ()

@end

@implementation SPHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     [self fetchAllCircleForUser:GETVALUE(@"userid")];
//    if(IS_IPHONE_5)
//    {
//        self.tblHomeView.frame=CGRectMake(0, self.tblHomeView.frame.origin.y, 320, self.tblHomeView.frame.size.height);
//        self.btnCreateCircle.frame=CGRectMake(0,525 , 320, 28);
//    }else{
//        self.tblHomeView.frame=CGRectMake(0, 80, 320,80);
//        self.btnCreateCircle.frame=CGRectMake(0,100 , 320,28);
//    }
    self.btnCreateCircle.titleLabel.font=OPENSANS_LIGHT(18);
}
#pragma mark - API CALL FetchCircle
-(void)fetchAllCircleForUser:(NSString *)strUserID
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:strUserID,@"userid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:KFETCHALLCIRCLE];
    //Send url to server
    [SPNetworkRequest getCircleListWithID:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            self.arrCircleList = [[NSMutableArray alloc]init];
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
             self.arrCircleList =[objParse parseCircleListResponse:response];
            [self.tblHomeView reloadData];
            [HUDCommon killHUD];
          
            
        });
        
        
    }];
    
}


#pragma mark  - TableViewDelegate -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self.arrCircleList count]>0) {
        
       return ([self.arrCircleList count]/2)+1;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SPHomeViewCell";
    SPHomeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (! cell) {
        cell = (SPHomeViewCell *) [[UIViewController alloc] initWithNibName:cellIdentifier bundle:nil];
    }
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    SPCircleListData *data1=nil;
    SPCircleListData *data2=nil;
    if(0+indexPath.row*2<[self.arrCircleList count])
        data1=[self.arrCircleList objectAtIndex:0+indexPath.row*2];
    if(1+indexPath.row*2<[self.arrCircleList count])
        data2=[self.arrCircleList objectAtIndex:1+indexPath.row*2];
   
    
    
    if (data1) {
        NSMutableArray *arrFirend = [[NSMutableArray alloc]initWithArray:data1.arrFriendsList];

        if ([arrFirend count]==0) {
            
            MycircleView *vw = (MycircleView *)[[UIViewController alloc] initWithNibName:@"MycircleView" bundle:nil].view;
            [vw setFrame:CGRectMake(10, 10, vw.frame.size.width, vw.frame.size.height)];
            [vw.btnCircleName setTitle:data1.strCircleName forState:UIControlStateNormal];
            vw.btnCircleName.titleLabel.font=OPENSANS(12);
            vw.btnCircleName.data=data1;
             [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
            
        }
        if ([arrFirend count]==1) {
            SPFriendsDetailsData *dataFriend =[arrFirend objectAtIndex:0];
            MyCircle1View *vw = (MyCircle1View *)[[UIViewController alloc] initWithNibName:@"MyCircle1View" bundle:nil].view;
            CALayer *imageLayer = vw.imgPic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend.strFriendImage)).length>0) {
             [vw.imgPic setImageURL:[NSURL URLWithString:(SafeString(dataFriend.strFriendImage))]];
             }
            [vw setFrame:CGRectMake(10, 10, vw.frame.size.width, vw.frame.size.height)];
            [vw.btnCircleName setTitle:data1.strCircleName forState:UIControlStateNormal];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
             vw.btnCircleName.data=data1;
            [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }
        if ([arrFirend count]==2) {
             SPFriendsDetailsData *dataFriend1 =[arrFirend objectAtIndex:0];
             SPFriendsDetailsData *dataFriend2 =[arrFirend objectAtIndex:1];
            MyCircle2View *vw = (MyCircle2View *)[[UIViewController alloc] initWithNibName:@"MyCircle2View" bundle:nil].view;
            CALayer *imageLayer = vw.img1Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend2.strFriendImage)).length>0) {
            NSString *url = SafeString(dataFriend1.strFriendImage);
            [vw.img1Pic setImageURL:[NSURL URLWithString:url]];
             }
             if ((SafeString(dataFriend2.strFriendImage)).length>0) {
            [vw.img2Pic setImageURL:[NSURL URLWithString:(SafeString(dataFriend2.strFriendImage))]];
             }
            imageLayer = vw.img2Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            [vw setFrame:CGRectMake(10, 10, vw.frame.size.width, vw.frame.size.height)];
            [vw.btnCircleName setTitle:data1.strCircleName forState:UIControlStateNormal];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
             vw.btnCircleName.data=data1;
            [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }
        if ([arrFirend count]==3) {
            SPFriendsDetailsData *dataFriend1 =[arrFirend objectAtIndex:0];
            SPFriendsDetailsData *dataFriend2 =[arrFirend objectAtIndex:1];
            SPFriendsDetailsData *dataFriend3 =[arrFirend objectAtIndex:2];
            MyCircle3View *vw = (MyCircle3View *)[[UIViewController alloc] initWithNibName:@"MyCircle3View" bundle:nil].view;
            CALayer *imageLayer = vw.img1Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend1.strFriendImage)).length>0) {
            [vw.img1Pic setImageURL:[NSURL URLWithString:SafeString(dataFriend1.strFriendImage)]];
             }
            if ((SafeString(dataFriend2.strFriendImage)).length>0) {
               [vw.img2Pic setImageURL:[NSURL URLWithString:SafeString( dataFriend2.strFriendImage)]];
            }
           
            imageLayer = vw.img2Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            if ((SafeString(dataFriend3.strFriendImage)).length>0) {
            [vw.img3Pic setImageURL:[NSURL URLWithString:dataFriend3.strFriendImage]];
              }
            imageLayer = vw.img3Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            [vw.btnCircleName setTitle:data1.strCircleName forState:UIControlStateNormal];
            [vw setFrame:CGRectMake(10, 10, vw.frame.size.width, vw.frame.size.height)];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
             vw.btnCircleName.data=data1;
            [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }

        if ([arrFirend count]>=4) {
            SPFriendsDetailsData *dataFriend1 =[arrFirend objectAtIndex:0];
            SPFriendsDetailsData *dataFriend2 =[arrFirend objectAtIndex:1];
            SPFriendsDetailsData *dataFriend3 =[arrFirend objectAtIndex:2];
            SPFriendsDetailsData *dataFriend4 =[arrFirend objectAtIndex:3];
            MyCircle4View *vw = (MyCircle4View *)[[UIViewController alloc] initWithNibName:@"MyCircle4View" bundle:nil].view;
            CALayer *imageLayer = vw.img1Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend1.strFriendImage)).length>0) {
            [vw.img1Pic setImageURL:[NSURL URLWithString:dataFriend1.strFriendImage]];
             }
             if ((SafeString(dataFriend2.strFriendImage)).length>0) {
            [vw.img2Pic setImageURL:[NSURL URLWithString:dataFriend2.strFriendImage]];
             }
            imageLayer = vw.img2Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend3.strFriendImage)).length>0) {
            [vw.img3Pic setImageURL:[NSURL URLWithString:dataFriend3.strFriendImage]];
             }
            imageLayer = vw.img3Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            if ((SafeString(dataFriend4.strFriendImage)).length>0) {
            [vw.img4Pic setImageURL:[NSURL URLWithString:dataFriend4.strFriendImage]];
            }
            imageLayer = vw.img4Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            [vw.btnCircleName setTitle:data1.strCircleName forState:UIControlStateNormal];
            [vw setFrame:CGRectMake(10, 10, vw.frame.size.width, vw.frame.size.height)];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
             vw.btnCircleName.data=data1;
            [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }


       

    }
    if (data2) {
        NSMutableArray *arrFirend2 = [[NSMutableArray alloc]initWithArray:data2.arrFriendsList];
         if ([arrFirend2 count]==0) {
           
             MycircleView *vw = (MycircleView *)[[UIViewController alloc] initWithNibName:@"MycircleView" bundle:nil].view;
             [vw setFrame:CGRectMake(160, 10, vw.frame.size.width, vw.frame.size.height)];
             [vw.btnCircleName setTitle:data2.strCircleName forState:UIControlStateNormal];
              vw.btnCircleName.titleLabel.font=OPENSANS(12);
              vw.btnCircleName.data=data2;
             [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
             [cell.contentView addSubview:vw];
         }
        if ([arrFirend2 count]==1) {
            SPFriendsDetailsData *dataFriend =[arrFirend2 objectAtIndex:0];
            MyCircle1View *vw = (MyCircle1View *)[[UIViewController alloc] initWithNibName:@"MyCircle1View" bundle:nil].view;
            CALayer *imageLayer = vw.imgPic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            if ((SafeString(dataFriend.strFriendImage)).length>0) {
            [vw.imgPic setImageURL:[NSURL URLWithString:(SafeString(dataFriend.strFriendImage))]];
            }
            [vw setFrame:CGRectMake(160, 10, vw.frame.size.width, vw.frame.size.height)];
            [vw.btnCircleName setTitle:data2.strCircleName forState:UIControlStateNormal];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
             vw.btnCircleName.data=data2;
             [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }
        if ([arrFirend2 count]==2) {
            SPFriendsDetailsData *dataFriend1 =[arrFirend2 objectAtIndex:0];
            SPFriendsDetailsData *dataFriend2 =[arrFirend2 objectAtIndex:1];
            MyCircle2View *vw = (MyCircle2View *)[[UIViewController alloc] initWithNibName:@"MyCircle2View" bundle:nil].view;
            CALayer *imageLayer = vw.img1Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend1.strFriendImage)).length>0) {
            NSString *url = SafeString(dataFriend1.strFriendImage);
            [vw.img1Pic setImageURL:[NSURL URLWithString:url]];
             }
             if ((SafeString(dataFriend2.strFriendImage)).length>0) {
            [vw.img2Pic setImageURL:[NSURL URLWithString:(SafeString(dataFriend2.strFriendImage))]];
             }
            imageLayer = vw.img2Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            [vw setFrame:CGRectMake(160, 10, vw.frame.size.width, vw.frame.size.height)];
            [vw.btnCircleName setTitle:data2.strCircleName forState:UIControlStateNormal];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
             vw.btnCircleName.data=data2;
             [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }
        if ([arrFirend2 count]==3) {
            SPFriendsDetailsData *dataFriend1 =[arrFirend2 objectAtIndex:0];
            SPFriendsDetailsData *dataFriend2 =[arrFirend2 objectAtIndex:1];
            SPFriendsDetailsData *dataFriend3 =[arrFirend2 objectAtIndex:2];
            MyCircle3View *vw = (MyCircle3View *)[[UIViewController alloc] initWithNibName:@"MyCircle3View" bundle:nil].view;
            CALayer *imageLayer = vw.img1Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend1.strFriendImage)).length>0) {
            [vw.img1Pic setImageURL:[NSURL URLWithString:dataFriend1.strFriendImage]];
             }
             if ((SafeString(dataFriend2.strFriendImage)).length>0) {
            [vw.img2Pic setImageURL:[NSURL URLWithString:dataFriend2.strFriendImage]];
             }
            imageLayer = vw.img2Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend3.strFriendImage)).length>0) {
            [vw.img3Pic setImageURL:[NSURL URLWithString:dataFriend3.strFriendImage]];
             }
            imageLayer = vw.img3Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            [vw setFrame:CGRectMake(160, 10, vw.frame.size.width, vw.frame.size.height)];
             vw.btnCircleName.titleLabel.font=OPENSANS(12);
            [vw.btnCircleName setTitle:data2.strCircleName forState:UIControlStateNormal];
             vw.btnCircleName.data=data2;
             [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }
        
        if ([arrFirend2 count]>=4) {
            SPFriendsDetailsData *dataFriend1 =[arrFirend2 objectAtIndex:0];
            SPFriendsDetailsData *dataFriend2 =[arrFirend2 objectAtIndex:1];
            SPFriendsDetailsData *dataFriend3 =[arrFirend2 objectAtIndex:2];
            SPFriendsDetailsData *dataFriend4 =[arrFirend2 objectAtIndex:3];
            MyCircle4View *vw = (MyCircle4View *)[[UIViewController alloc] initWithNibName:@"MyCircle4View" bundle:nil].view;
            CALayer *imageLayer = vw.img1Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend1.strFriendImage)).length>0) {
            [vw.img1Pic setImageURL:[NSURL URLWithString:dataFriend1.strFriendImage]];
             }
             if ((SafeString(dataFriend2.strFriendImage)).length>0) {
            [vw.img2Pic setImageURL:[NSURL URLWithString:dataFriend2.strFriendImage]];
             }
            imageLayer = vw.img2Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend3.strFriendImage)).length>0) {
            [vw.img3Pic setImageURL:[NSURL URLWithString:dataFriend3.strFriendImage]];
             }
            imageLayer = vw.img3Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
             if ((SafeString(dataFriend4.strFriendImage)).length>0) {
            [vw.img4Pic setImageURL:[NSURL URLWithString:dataFriend4.strFriendImage]];
             }
            imageLayer = vw.img4Pic.layer;
            [imageLayer setCornerRadius:22];
            [imageLayer setBorderWidth:0];
            [imageLayer setMasksToBounds:YES];
            
            [vw setFrame:CGRectMake(160, 10, vw.frame.size.width, vw.frame.size.height)];
            vw.btnCircleName.titleLabel.font=OPENSANS(12);
            [vw.btnCircleName setTitle:data2.strCircleName forState:UIControlStateNormal];
             vw.btnCircleName.data=data2;
             [vw.btnCircleName addTarget:self action:@selector(circleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:vw];
        }

    }
    
    return cell;
}
-(IBAction)circleButtonAction:(id)sender
{
    int adminId = [GETVALUE(@"userid") intValue] ;
    //int adminId=2;
    SPHomeViewButton *btn =(SPHomeViewButton *)sender;
    NSLog(@"button circleName = %@",btn.data.strCircleName);
    NSLog(@"button circleId = %@",btn.data.strCircleId);
    NSLog(@"button circleAdmin = %@",btn.data.strCircleAdminID);
    SPCircleDetailsViewController *obj = [[SPCircleDetailsViewController alloc] initWithCollectionViewLayout:[[CircleLayout alloc] init]];
    obj.strCircleId=btn.data.strCircleId;
    obj.strCircleName=btn.data.strCircleName;
    obj.adminId=[btn.data.strCircleAdminID intValue];
    if (adminId == [btn.data.strCircleAdminID intValue] ) {
        obj.isAdmin=YES;
    }else{
         obj.isAdmin=NO;
    }
    [self.navigationController pushViewController:obj animated:YES];
    
}
-(IBAction)btnSettingClick:(id)sender
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
