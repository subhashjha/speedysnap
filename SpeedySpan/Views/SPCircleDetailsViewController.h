//
//  SPCircleDetailsViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIActionSheetViewController.h"
#import "iCarousel.h"
@interface SPCircleDetailsViewController : UICollectionViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomSheetDelegate,iCarouselDataSource,iCarouselDelegate,UIAlertViewDelegate>
{
     UIView *circleView;
     UILabel *navTitle;
}
@property(nonatomic,strong)UILabel *lblNoPhoto;
@property(nonatomic, retain)  CustomUIActionSheetViewController *customUIActionSheetViewController;

@property (nonatomic, assign) int adminId;
@property (nonatomic, assign) BOOL isAdmin;
@property (nonatomic, assign) BOOL delCircle;
@property(nonatomic,assign) BOOL fromCircle;
@property (nonatomic, assign) NSInteger cellCount;
@property (nonatomic, strong) NSString *strCircleId;
@property (nonatomic, strong) NSString *strCircleName;
@property (nonatomic ,strong) NSIndexPath* tappedCellPath;
@property (nonatomic, strong) NSMutableArray *arrCircleList;
@property (nonatomic, strong) UIActionSheet *actionSheet;
@property (nonatomic, strong) iCarousel *carousel;
@property (nonatomic, strong) NSMutableArray *arrImageList;
@end
