//
//  SPSelectMemberViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPSelectMemberViewController.h"
#import "SPSelectMemberCell.h"
#import <QuartzCore/QuartzCore.h>
#import "SPFriendListData.h"
#import "SPHomeViewController.h"
@interface SPSelectMemberViewController ()

@end

@implementation SPSelectMemberViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.arrFriendList = [[NSMutableArray alloc]init];
    self.arrSelectedFrnd = [[NSMutableArray alloc]init];
	// Do any additional setup after loading the view.
     self.lblTitle.font=OPENSANS(22);
//    if(IS_IPHONE_5)
//    {
//        self.tblMember.frame=CGRectMake(0, self.tblMember.frame.origin.y, 320, 560);
//    }else{
//        self.tblMember.frame=CGRectMake(0, self.tblMember.frame.origin.y, 320,300);
//    }
    [self fetchFriendList];
}
-(IBAction)btnBackClk:(id)sender
{
    if (self.fromDetailsView) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self performSegueWithIdentifier:@"memberToHomeViewController" sender:self];
    }
    
}
#pragma mark Seguue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"memberToHomeViewController"]) {
        
        
        
    }
    
    
}
-(void)fetchFriendList
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
   
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:GETVALUE(@"userid"),@"userid",GETVALUE(@"fbAccessToken"),@"accesstoken",GETVALUE(@"socialid"),@"socialid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kFETCHFRIENDLIST];
    //Send url to server
    [SPNetworkRequest getFriendListWithUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
           self.arrFriendList =[objParse parseFriendListResponse:response];
            if ([self.arrFriendList count]>0) {
                [self.tblMember reloadData];
            }else{
                [HUDCommon showMessage:@"Friend list is empty" withColor:kRED showOnTOP:YES];
            }
            //Push create circle view
            [HUDCommon killHUD];
          
            
            
            
        });
        
        
    }];
    
    
}

#pragma mark -
#pragma mark UITableViewDelegate and UITableViewDataSource
#pragma mark -


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrFriendList count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SPSelectMemberCell";
    
    SPSelectMemberCell *cell = (SPSelectMemberCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[SPSelectMemberCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
      
    }
    CALayer *imageLayer = cell.profileImageView.layer;
    [imageLayer setCornerRadius:22];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    //Get list data from array
    SPFriendListData *data = [self.arrFriendList objectAtIndex:indexPath.row];
    cell.lblName.font=OPENSANS(16);
    cell.lblName.text=SafeString(data.strName);
    if ([SafeString(data.strStatus) isEqualToString:@"INVITE"]) {
        [cell.btnAdd setTitle:@"INVITE" forState:UIControlStateNormal];
    }else{
        [cell.btnAdd setTitle:@"ADD" forState:UIControlStateNormal];
    }
    cell.profileImageView.image=nil;
    [cell.profileImageView setImageURL:[NSURL URLWithString:(SafeString(data.strImageUrl))]];
    cell.btnAdd.tag = indexPath.row;
    [cell.btnAdd addTarget:self action:@selector(btnAddOrInviteClk:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}
-(void)btnAddOrInviteClk:(id)sender
{
     UIButton *selectedbutton=(UIButton *)sender;
    NSLog(@"sectectBtn tag %d",selectedbutton.tag);
    NSLog(@"sectectBtn tag %@",selectedbutton.titleLabel.text);
    if ([selectedbutton.titleLabel.text isEqualToString:@"INVITE"]) {
        [[NSBundle mainBundle] loadNibNamed:@"SPAlertPopupView" owner:self options:nil];
        [self.view addSubview:self.viewPopup];
        self.lblPopupTitle.font=OPENSANS_LIGHT(16);
        self.txtfldEmail.font=OPENSANS_LIGHT(16);
        self.btnCancel.titleLabel.font=OPENSANS_LIGHT(18);
        self.btnSend.titleLabel.font=OPENSANS_LIGHT(18);
        
        [self.txtfldEmail becomeFirstResponder];
        [UIView animateWithDuration:0.2 animations:
         ^(void){
             self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
             self.viewPopup.alpha = 0.5;
         }
                         completion:^(BOOL finished){
                             [self bounceOutAnimationStoped];
                         }];
    }else{
        //Get list data from array
        SPFriendListData *data = [self.arrFriendList objectAtIndex:selectedbutton.tag];
        if(![self.arrSelectedFrnd containsObject:data.strId]){
        [self.arrSelectedFrnd addObject:data.strId];
        }else{
            [HUDCommon showMessage:@"Already added in list." withColor:kRED showOnTOP:YES];
        }
    }
    
}
-(IBAction)btnAddFriendClk:(id)sender
{
    if (self.arrSelectedFrnd.count>0) {
        
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    NSString *strSocialId = [self.arrSelectedFrnd componentsJoinedByString:@","];
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:GETVALUE(@"userid"),@"userid",strSocialId,@"socialid",GETVALUE(@"circleid"),@"circleid", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kADDFriend];
    //Send url to server
    [SPNetworkRequest AddfriendToCircle:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
             [HUDCommon killHUD];
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            NSString *strResponse =[objParse parseUploadImageResponse:response];
                NSString *strMsg;
                if ([strResponse intValue]==0) {
                    strMsg = @"Friend added successfully in your circle.";
                      [HUDCommon showMessage:strMsg withColor:kGREEN showOnTOP:YES];
                }else  if ([strResponse intValue]==1) {
                     strMsg = @"Friend already added in your circle.";
                      [HUDCommon showMessage:strMsg withColor:kRED showOnTOP:YES];
                }else{
                 strMsg = [NSString stringWithFormat:@"%@ user already added in your circle",strResponse];
                      [HUDCommon showMessage:strMsg withColor:kRED showOnTOP:YES];
                }
               
            
                 AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                appDelgObj.isDeleteImage=YES;
                [self.arrSelectedFrnd removeAllObjects];
            
                
          
            //Push create circle view
           
            
            
            
            
        });
        
        
    }];
    }else{
        [HUDCommon showMessage:@"Please select friend to add" withColor:kRED showOnTOP:YES];
    }
  
}
-(IBAction)btnCancelPopupClk:(id)sender
{
    [self.txtfldEmail resignFirstResponder];
    // ---------- *** remove popupView ***--------------------
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
         self.viewPopup.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bouncePopOutAnimationStoped];
                     }];
}
-(IBAction)btnSendClk:(id)sender
{
    if (self.txtfldEmail.text.length>0) {
        if(![self isValidEmail:self.txtfldEmail.text])
        {
           [HUDCommon showMessage:@"Please enter valid Email address." withColor:kRED showOnTOP:YES];
        }else{
            [self sendInvitation:self.txtfldEmail.text];
        }
        
    }else{
        [HUDCommon showMessage:@"Please enter Email address." withColor:kRED showOnTOP:YES];
    }
    
}
-(void)sendInvitation:(NSString *)emailAddess
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:GETVALUE(@"userid"),@"userid",emailAddess,@"email", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kSENDINVITATION];
    //Send url to server
    [SPNetworkRequest sendInvitation:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
           NSString *strResponse =[objParse parseResponseString:response];
            if ([strResponse isEqualToString:@"yes"]) {
              [HUDCommon showMessage:@"Invitation successfully send." withColor:kGREEN showOnTOP:YES];
                [self.txtfldEmail resignFirstResponder];
                // ---------- *** remove popupView ***--------------------
                [UIView animateWithDuration:0.2 animations:
                 ^(void){
                     self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.1f, 1.1f);
                     self.viewPopup.alpha = 0.8;
                 }
                                 completion:^(BOOL finished){
                                     [self bouncePopOutAnimationStoped];
                                 }];
                
            }else{
                [HUDCommon showMessage:@"Invitation not send.Please try again later." withColor:kRED showOnTOP:YES];
            }
            //Push create circle view
            [HUDCommon killHUD];
            
            
            
            
        });
        
        
    }];

}

#pragma mark View weight Annimation
#pragma mark-
- (void)bounceOutAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9, 0.9);
         self.viewPopup.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bounceInAnimationStoped];
                     }];
}

- (void)bounceInAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
         self.viewPopup.alpha = 1.0;
     }
                     completion:^(BOOL finished){
                         [self animationStoped];
                     }];
}

- (void)bouncePopOutAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.0, 0.0);
         self.viewPopup.alpha = 0.0;
     }
                     completion:^(BOOL finished){
                         [self.viewPopup removeFromSuperview];
                     }];
}
- (void)animationStoped
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma markEmail validation ::::::::::::::::::::::::::::::::::::::::::::
-(BOOL) isValidEmail:(NSString *)checkString
{
	BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
	NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
	NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
