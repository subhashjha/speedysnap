//
//  SPCreateCircleViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPCreateCircleViewController.h"

@interface SPCreateCircleViewController ()

@end

@implementation SPCreateCircleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //Set navigation bar hidden =NO
   // [[self navigationController] setNavigationBarHidden:YES animated:NO];
    self.lblTitle.font=OPENSANS(22);
    self.btnCreateCircle.titleLabel.font=OPENSANS_LIGHT(18);
    
}
-(IBAction)btnHelpClk:(id)sender
{
    
}
-(IBAction)btnCreateCircleClk:(id)sender
{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
