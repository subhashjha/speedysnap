//
//  SPCreateCircleFromViewController.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPCreateCircleFromViewController.h"

@interface SPCreateCircleFromViewController ()

@end

@implementation SPCreateCircleFromViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setFonts];
}
-(void)setFonts
{
    self.lblTitle.font=OPENSANS(22);
    self.btnSelectMenber.titleLabel.font=OPENSANS_LIGHT(18);
    self.lblCircleName.font=OPENSANS_LIGHT(16);
    self.txtFieldCircle.font=OPENSANS_LIGHT(16);
}
#pragma mark UIButtonAction
-(IBAction)btnSelectMemeberClk:(id)sender
{
    if (self.txtFieldCircle.text.length>0) {
       //  [self performSegueWithIdentifier:@"selectMember" sender:self];
        [self createCircle:self.txtFieldCircle.text];
    }else{
        //Show alert view
        [HUDCommon showMessage:@"Please enter circle name." withColor:kRED showOnTOP:YES];
    }
}
-(void)createCircle:(NSString *)circleName
{
    [HUDCommon showHUDWithTitle:@"Please wait" withSubTitle:nil];
    // Do any additional setup after loading the view.
    SPCreateUrl *objUrl =[[SPCreateUrl alloc]init];
    //Genrate url with data
    NSString *userId = GETVALUE(@"userid");
    NSMutableDictionary *dicValue = [[NSMutableDictionary alloc]initWithObjectsAndKeys:userId,@"admin",circleName,@"circlename", nil];
    NSString *url=[objUrl CreateUrlWithDetails:dicValue withType:kCREATECIRCLE];
    //Send url to server
    [SPNetworkRequest createCircleWithUrl:url withParameter:nil completionHandler:^(NSString *response, NSError *error){
        // Back to main thread
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"response = %@",response);
            SPParserJsonResponse *objParse = [[SPParserJsonResponse alloc]init];
            NSString *strResponse =[objParse parseCreateCircleResponse:response];
            //Push create circle view
            [HUDCommon killHUD];
            if ([strResponse isEqualToString:@"yes"]) {
               [self performSegueWithIdentifier:@"selectMember" sender:self];
            }else{
               // [self performSegueWithIdentifier:@"selectMember" sender:self];
                [HUDCommon showMessage:strResponse withColor:kRED showOnTOP:YES];
            }
            
            
            
        });
        
        
    }];

    
}
-(IBAction)btnBackClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)btnOutSideClk:(id)sender
{
    [self.txtFieldCircle resignFirstResponder];
  
}
#pragma mark Seguue
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"selectMember"]) {
        
        
        
    }
    
    
}

#pragma mark UITextfieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
