//
//  ViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 25/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(nonatomic,strong)IBOutlet UIButton *btnFacebook;
-(IBAction)loginWithFBBtnClk:(id)sender;
-(IBAction)loginWithFBBtnClk1:(id)sender;
@end
