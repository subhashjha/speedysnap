//
//  SPSettingsViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPSettingsViewController : UIViewController
{
   BOOL selected;
}
@property(nonatomic,strong)IBOutlet UILabel *lblTitle;
@property(nonatomic,strong)IBOutlet UILabel *lblNotification;
@property(nonatomic,strong)IBOutlet UILabel *lblRateApp;
@property(nonatomic,strong)IBOutlet UIButton *btnNotification;
@property(nonatomic,strong)IBOutlet UIButton *btnlogout;
-(IBAction)btnRateAppClk:(id)sender;
-(IBAction)btnNotificationClk:(id)sender;
-(IBAction)btnLogoutClk:(id)sender;
-(IBAction)btnBackClk:(id)sender;
@end
