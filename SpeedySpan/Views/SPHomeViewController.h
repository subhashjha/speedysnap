//
//  SPHomeViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 03/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPHomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)IBOutlet UITableView *tblHomeView;
@property(nonatomic,strong) NSMutableArray *arrCircleList;
@property(nonatomic,strong)IBOutlet UIButton *btnCreateCircle;
-(IBAction)btnSettingClick:(id)sender;
@end
