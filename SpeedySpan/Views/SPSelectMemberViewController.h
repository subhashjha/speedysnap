//
//  SPSelectMemberViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPSelectMemberViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property(nonatomic,strong)IBOutlet UILabel *lblTitle;
@property(nonatomic,strong)IBOutlet UITableView *tblMember;
@property(nonatomic,strong)NSMutableArray *arrFriendList;
@property(nonatomic,strong)NSMutableArray *arrSelectedFrnd;
@property(nonatomic,assign)BOOL fromDetailsView;

@property(nonatomic,strong)IBOutlet UIView *viewPopup;
@property(nonatomic,strong)IBOutlet UIView *viewPopupContainer;
@property(nonatomic,strong)IBOutlet UILabel *lblPopupTitle;
@property(nonatomic,strong)IBOutlet UITextField *txtfldEmail;
@property(nonatomic,strong)IBOutlet UIButton *btnCancel;
@property(nonatomic,strong)IBOutlet UIButton *btnSend;
-(IBAction)btnBackClk:(id)sender;
-(IBAction)btnCancelPopupClk:(id)sender;
-(IBAction)btnSendClk:(id)sender;
-(IBAction)btnAddFriendClk:(id)sender;
@end
