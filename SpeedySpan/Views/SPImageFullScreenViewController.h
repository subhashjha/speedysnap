//
//  SPImageFullScreenViewController.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "CustomUIActionSheetViewController.h"
@interface SPImageFullScreenViewController : UIViewController<iCarouselDataSource, iCarouselDelegate,UIAlertViewDelegate,CustomSheetDelegate>
@property(nonatomic, retain) IBOutlet CustomUIActionSheetViewController *customUIActionSheetViewController;
@property(nonatomic,strong)NSMutableArray *arrImageList;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSString *circleName;
@property (assign, nonatomic) int adminId;
@property (assign, nonatomic) BOOL isAdmin;
@property (assign, nonatomic) BOOL isFromPush;
@property (strong, nonatomic) NSString *circleID;
@property (assign, nonatomic) NSInteger selectedIndex;
-(IBAction)btnBackClk:(id)sender;
@end
