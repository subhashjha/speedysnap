//
//  AppDelegate.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 25/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <CoreLocation/CoreLocation.h>
#import "MBProgressHUD.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

 @property (strong, nonatomic)NSMutableDictionary *dicNotificationInfo;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) FBSession *session;
@property (strong, nonatomic) MBProgressHUD *HUD;
@property(nonatomic,retain)CLLocationManager *locationManager;
@property(assign)CLLocationCoordinate2D userLocation;
@property(nonatomic,assign) BOOL isDeleteImage;
-(void)FacebookLogOut;
@end
