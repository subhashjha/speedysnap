//
//  SPHomeViewCell.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 03/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPHomeViewCell : UITableViewCell
@property(nonatomic,strong)IBOutlet UIImageView *profileImageView;
@property(nonatomic,strong)IBOutlet UILabel *lblCircleName;
@property(nonatomic,strong)IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnCircle2;
@property (weak, nonatomic) IBOutlet UIButton *btnCircle1;
@end
