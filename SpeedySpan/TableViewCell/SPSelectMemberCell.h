//
//  SPSelectMemberCell.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 26/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPSelectMemberCell : UITableViewCell

@property(nonatomic,strong)IBOutlet UIImageView *profileImageView;
@property(nonatomic,strong)IBOutlet UILabel *lblName;
@property(nonatomic,strong)IBOutlet UIButton *btnAdd;
@end
