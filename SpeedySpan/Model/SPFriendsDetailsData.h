//
//  SPFriendsDetailsData.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 03/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPFriendsDetailsData : NSObject
@property(nonatomic,strong)NSString *strFriendID;
@property(nonatomic,strong)NSString *strFriendImage;
@end
