//
//  SPFriendListData.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPFriendListData : NSObject

@property(nonatomic,strong)NSString *strStatus;
@property(nonatomic,strong)NSString *strName;
@property(nonatomic,strong)NSString *strId;
@property(nonatomic,strong)NSString *strImageUrl;


@end
