//
//  SPCircleListData.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 03/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPCircleListData : NSObject
@property(nonatomic,strong)NSString *strCircleId;
@property(nonatomic,strong)NSMutableArray *arrCircleImage;
@property(nonatomic,strong)NSMutableArray *arrFriendsList;
@property(nonatomic,strong)NSString *strCircleName;
@property(nonatomic,strong)NSString *strCircleAdminID;
@end
