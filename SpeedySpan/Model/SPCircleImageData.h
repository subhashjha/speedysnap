//
//  SPCircleImageData.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 03/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPCircleImageData : NSObject

@property(nonatomic,retain)NSString *strLatitude;
@property(nonatomic,retain)NSString *strLongitude;
@property(nonatomic,strong)NSString *strImageUrl;
@property(nonatomic,retain)NSString *strUploadedBy;
@property(nonatomic,strong)NSString *strImageId;
@property(nonatomic,strong)NSString *strUploaderName;
@property(nonatomic,strong)UIImage *image;
@property(nonatomic,strong)UIImageView *imageView;
@end
