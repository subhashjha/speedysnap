//
//  SPHomeViewButton.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPCircleListData.h"
@interface SPHomeViewButton : UIButton
@property (nonatomic,strong)SPCircleListData *data;
@end
