//
//  SPHomeViewButton.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 04/03/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPHomeViewButton.h"

@implementation SPHomeViewButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
