//
//  SPParserJsonResponse.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPParserJsonResponse.h"
#import "SBJSON.h"
#import "SPFriendListData.h"
#import "SPCircleListData.h"
#import "SPFriendsDetailsData.h"
#import "SPCircleImageData.h"
@implementation SPParserJsonResponse
-(NSString *) parseRegistrationResponse: (NSString *) response
{
 
    if([response length] > 0)
	{
		SBJSON *parser = [[SBJSON alloc] init];
		
		NSDictionary *dict = [parser objectWithString:response error:nil];
		if([[dict objectForKey:@"errorMsg"] isEqualToString:@"success"])
        {
            UPDATEVALUE(@"userid", [dict objectForKey:@"userid"]);
            UPDATEVALUE(@"circlecount", [dict objectForKey:@"count"]);
            return @"yes";
            
        }else
        {
            return [dict objectForKey:@"errorMsg"];
        }
		
	}
	return @"";
}
-(NSString *) parseCreateCircleResponse: (NSString *) response
{
    if([response length] > 0)
	{
		SBJSON *parser = [[SBJSON alloc] init];
		
		NSDictionary *dict = [parser objectWithString:response error:nil];
		if([[dict objectForKey:@"errorMsg"] isEqualToString:@"success"])
        {
            UPDATEVALUE(@"circleid", [dict objectForKey:@"circleid"]);
            return @"yes";
            
        }else
        {
            return [dict objectForKey:@"errorMsg"];
        }
		
	}
	return @"";
}
-(NSString *) parseUploadImageResponse: (NSString *) response
{
    if([response length] > 0)
	{
		SBJSON *parser = [[SBJSON alloc] init];
		
		NSDictionary *dict = [parser objectWithString:response error:nil];
		
           
            return [dict objectForKey:@"errorMsg"];
            
        
		
	}
	return @"";
}
-(NSString *) parseResponseString: (NSString *) response
{
    if([response length] > 0)
	{
		SBJSON *parser = [[SBJSON alloc] init];
		
		NSDictionary *dict = [parser objectWithString:response error:nil];
		if([[dict objectForKey:@"errorMsg"] isEqualToString:@"success"])
        {
            return @"yes";
            
        }else
        {
            return [dict objectForKey:@"errorMsg"];
        }
		
	}
	return @"";
}

-(NSMutableArray *) parseFriendListResponse:(NSString *) response
{
    NSMutableArray *arrFriendList = [[NSMutableArray alloc]init];
    NSMutableArray *aryUnique = [[NSMutableArray alloc]init];
    NSMutableArray *arrFriendListTemp1 = [[NSMutableArray alloc]init];
    NSMutableArray *arrFriendListTemp2 = [[NSMutableArray alloc]init];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([response length] > 0)
    {
        SBJSON *parser = [[SBJSON alloc] init];
       NSDictionary *dict = [parser objectWithString:response error:nil];
        NSMutableArray *arrGroups = [dict objectForKey:@"data" ];
            for (NSDictionary *dic in arrGroups)
			{
                SPFriendListData *objFriendLsit = [[SPFriendListData alloc] init];
                objFriendLsit.strId=[dic objectForKey:@"id"];
                objFriendLsit.strName=[dic objectForKey:@"name"];
                objFriendLsit.strStatus=[dic objectForKey:@"status"];
                objFriendLsit.strImageUrl=[dic objectForKey:@"image"];
                [arrFriendListTemp1 addObject:[dic objectForKey:@"name"]];
                [arrFriendListTemp2 addObject:objFriendLsit];

                //[arrFriendList addObject:objFriendLsit];
                
            }
           [aryUnique addObjectsFromArray:[[NSSet setWithArray:arrFriendListTemp1] allObjects]];
          NSArray *sortedPersonArray = [aryUnique sortedArrayUsingSelector:@selector(compare:)];
        for (int x=0; x<[sortedPersonArray count]; x++)
        {
            NSString *name = [sortedPersonArray objectAtIndex:x];
            for (int y = 0; y<[arrFriendListTemp2 count]; y++)
            {
                SPFriendListData *objData = [arrFriendListTemp2 objectAtIndex:y];
                if ([objData.strName isEqualToString:name])
                {
                    [arrFriendList addObject:objData];
                }
            }
        }
        return arrFriendList ;
        
    
    }
    return nil ;
}
-(NSMutableArray *)parseCircleListWithCircleIDResponse:(NSString *) response
{
    
    NSMutableArray *arrCircleList = [[NSMutableArray alloc]init];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([response length] > 0)
    {
        SBJSON *parser = [[SBJSON alloc] init];
        NSLog(@"response = %@",response);
        if([response rangeOfString:@"["].location==0){
            NSMutableArray *arrGroups = [parser objectWithString:response error:nil];
            for (NSDictionary *dic in arrGroups)
			{
                SPCircleListData *objCircleListData = [[SPCircleListData alloc] init];
                objCircleListData.arrFriendsList = [[NSMutableArray alloc]init];
                objCircleListData.strCircleId=[dic objectForKey:@"circleid"];
                objCircleListData.strCircleName=[dic objectForKey:@"circlename"];
                objCircleListData.strCircleAdminID=[dic objectForKey:@"circleadmin"];
                objCircleListData.arrCircleImage=[[NSMutableArray alloc]init];
                NSMutableArray *arrFriends =[dic objectForKey:@"friends"];
                NSMutableArray *arrImages =[dic objectForKey:@"images"];
                
                for (NSDictionary *dicFriends in arrFriends)
                {
                    SPFriendsDetailsData *objFriendList = [[SPFriendsDetailsData alloc]init];
                    objFriendList.strFriendID=[dicFriends objectForKey:@"friendid"];
                      objFriendList.strFriendImage=[dicFriends objectForKey:@"friendimage"];
                    [objCircleListData.arrFriendsList addObject:objFriendList];
                }
                for (NSDictionary *dicImages in arrImages)
                {
                    SPCircleImageData *objImageList = [[SPCircleImageData alloc]init];
                    objImageList.strLatitude=[dicImages objectForKey:@"imagelat"];
                    objImageList.strLongitude=[dicImages objectForKey:@"imagelong"];
                    objImageList.strImageUrl=[dicImages objectForKey:@"imagepath"];
                    objImageList.strUploadedBy=[dicImages objectForKey:@"uploadedby"];
                    objImageList.strImageId=[dicImages objectForKey:@"imageid"];
                    objImageList.strUploaderName=[dicImages objectForKey:@"addedby"];
                    [objCircleListData.arrCircleImage addObject:objImageList];
                }
               // objCircleListData.arrFriendsList=[dic objectForKey:@"friends"];
                [arrCircleList addObject:objCircleListData];
                
            }
            return arrCircleList ;
        }
        else {
			return nil;
		}
    }
    return nil ;
}
-(NSMutableArray *)parseCircleListResponse:(NSString *) response
{
    
    NSMutableArray *arrCircleList = [[NSMutableArray alloc]init];
    response = [response stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if([response length] > 0)
    {
        SBJSON *parser = [[SBJSON alloc] init];
        if([response rangeOfString:@"["].location==0){
            NSMutableArray *arrGroups = [parser objectWithString:response error:nil];
            for (NSArray *arr in arrGroups)
			{
                for (NSDictionary *dic in arr)
                {
                SPCircleListData *objCircleListData = [[SPCircleListData alloc] init];
                objCircleListData.arrFriendsList = [[NSMutableArray alloc]init];
                objCircleListData.strCircleId=[dic objectForKey:@"circleid"];
                objCircleListData.strCircleName=[dic objectForKey:@"circlename"];
                objCircleListData.strCircleAdminID=[dic objectForKey:@"circleadmin"];
                objCircleListData.arrCircleImage=[[NSMutableArray alloc]init];
                NSMutableArray *arrFriends =[dic objectForKey:@"friends"];
                NSMutableArray *arrImages =[dic objectForKey:@"images"];
                
                for (NSDictionary *dicFriends in arrFriends)
                {
                    SPFriendsDetailsData *objFriendList = [[SPFriendsDetailsData alloc]init];
                    objFriendList.strFriendID=[dicFriends objectForKey:@"friendid"];
                    objFriendList.strFriendImage=[dicFriends objectForKey:@"friendimage"];
                    [objCircleListData.arrFriendsList addObject:objFriendList];
                }
                for (NSDictionary *dicImages in arrImages)
                {
                    SPCircleImageData *objImageList = [[SPCircleImageData alloc]init];
                    objImageList.strLatitude=[dicImages objectForKey:@"imagelat"];
                    objImageList.strLongitude=[dicImages objectForKey:@"imagelong"];
                    objImageList.strImageUrl=[dicImages objectForKey:@"imagepath"];
                    objImageList.strUploadedBy=[dicImages objectForKey:@"uploadedby"];
                    objImageList.strImageId=[dicImages objectForKey:@"imageid"];
                     objImageList.strUploaderName=[dicImages objectForKey:@"addedby"];
                    [objCircleListData.arrCircleImage addObject:objImageList];
                }
                // objCircleListData.arrFriendsList=[dic objectForKey:@"friends"];
                [arrCircleList addObject:objCircleListData];
                
            }
            }
            return arrCircleList ;
        }
        else {
			return nil;
		}
    }
    return nil ;
}

@end
