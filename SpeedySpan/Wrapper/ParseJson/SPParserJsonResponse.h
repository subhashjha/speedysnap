//
//  SPParserJsonResponse.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPParserJsonResponse : NSObject
-(NSString *) parseRegistrationResponse: (NSString *) response;
-(NSString *) parseCreateCircleResponse: (NSString *) response;
-(NSMutableArray *) parseFriendListResponse:(NSString *) response;
-(NSMutableArray *)parseCircleListWithCircleIDResponse:(NSString *) response;
-(NSString *) parseUploadImageResponse: (NSString *) response;
-(NSMutableArray *)parseCircleListResponse:(NSString *) response;
-(NSString *)parseResponseString: (NSString *) response;
@end
