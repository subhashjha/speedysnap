
#import <UIKit/UIKit.h>
@protocol CustomSheetDelegate <NSObject>

@required
-(void)selectedButton:(int)tag;

@end
@interface CustomUIActionSheetViewController : UIViewController {
	UIView *actionSheetView;
}

@property(nonatomic, retain) IBOutlet UIView *actionSheetView;
@property(nonatomic, retain) IBOutlet UIView *viewActionSheet;
@property(nonatomic,strong) id<CustomSheetDelegate> delegate;
- (IBAction)slideOut:(id)sender;
- (IBAction)cameraClk:(id)sender;
- (IBAction)galloryClk:(id)sender;
- (IBAction)showFullImage:(id)sender;
- (IBAction)removeImage:(id)sender;


- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)btnSaveImageAction:(id)sender;
- (IBAction)btnDeleteImageAction:(id)sender;

@end
