//
//  HUDCommon.m
//  DuckFace
//
//  Created by Team Teknowledge on 2/12/13.
//  Copyright (c) 2013 Teknowledge Software. All rights reserved.
//

#import "HUDCommon.h"
@implementation HUDCommon
#pragma mark -
#pragma mark Progress HUD

+ (void)showHUD:(NSString*)strTitle {
	
    //DLog(@"Show HUD Enter");
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
	if (! appDelgObj.HUD.superview )
	{
		appDelgObj.HUD = [[MBProgressHUD alloc] initWithWindow:appDelgObj.window] ;
		[appDelgObj.window addSubview:appDelgObj.HUD];
		appDelgObj.HUD.labelText = @"Please Wait";

		appDelgObj.HUD.detailsLabelText = strTitle;

		[appDelgObj.HUD show:YES];
	}    
}

+(void)showHUDWithTitle:(NSString *)title withSubTitle:(NSString *)subTitle{
	
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
	appDelgObj.HUD=[[MBProgressHUD alloc]initWithWindow:appDelgObj.window];
	[appDelgObj.window addSubview:appDelgObj.HUD];
    
    // Set the hud to display with a color
    appDelgObj.HUD.color = HUD_COLOR;
    appDelgObj.HUD.dimBackground = YES;
    
    appDelgObj.HUD.userInteractionEnabled=NO;
	
    appDelgObj.HUD.labelText=title;
	appDelgObj.HUD.detailsLabelText=subTitle;
    
    appDelgObj.window.userInteractionEnabled=NO;
    
	[appDelgObj.HUD show:YES];
}


+ (void)showHUD{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	if (! appDelgObj.HUD.superview )
	{
		appDelgObj.HUD = [[MBProgressHUD alloc] initWithWindow:appDelgObj.window] ;
		[appDelgObj.window addSubview:appDelgObj.HUD];
		appDelgObj.HUD.labelText = @"";//@"Loading...";
		appDelgObj.HUD.detailsLabelText = @"";
		[appDelgObj.HUD show:YES];
	}    
}

+(void)showHUDWithPercentage:(double)percentage WithText:(NSString *)text{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	if (! appDelgObj.HUD.superview )
	{
		appDelgObj.HUD = [[MBProgressHUD alloc] initWithWindow:appDelgObj.window] ;
        appDelgObj.HUD.progress=percentage;
        appDelgObj.HUD.mode=MBProgressHUDModeDeterminate;
        appDelgObj.HUD.labelText=text;
		[appDelgObj.window addSubview:appDelgObj.HUD];
        
    }
    else{
        appDelgObj.HUD.mode=MBProgressHUDModeDeterminate;
        appDelgObj.HUD.progress=percentage;
        appDelgObj.HUD.mode=MBProgressHUDModeDeterminate;
        appDelgObj.HUD.labelText=text;
    }
}

+ (void)showHUDinView:(UIView *)view withTitle:(NSString*)strTitle {
	
       // DLog(@"Show HUD Enter");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	if (! appDelgObj.HUD.superview )
	{
		appDelgObj.HUD = [[MBProgressHUD alloc] initWithView:view] ;
		[view addSubview:appDelgObj.HUD];
		appDelgObj.HUD.labelText = @"Please Wait...";//@"Loading...";
		appDelgObj.HUD.detailsLabelText = strTitle;
		[appDelgObj.HUD show:YES];
	}    
}

+ (void)showHUDWithColor:(UIColor *)HUDcolor{
    
    AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
	if (! appDelgObj.HUD.superview )
	{
		appDelgObj.HUD = [[MBProgressHUD alloc] initWithWindow:appDelgObj.window] ;
		[appDelgObj.window addSubview:appDelgObj.HUD];
		appDelgObj.HUD.labelText =@"Please Wait...";
		appDelgObj.HUD.color=HUDcolor;
		[appDelgObj.HUD show:YES];
	}
}

+ (void)killHUD{
	
    //DLog(@"Kill HUD Enter");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	if ( appDelgObj.HUD.superview){
        [appDelgObj.HUD hide:YES];
        appDelgObj.window.userInteractionEnabled=YES;
        [appDelgObj.HUD removeFromSuperview];
	}
}

#pragma mark -
#pragma mark alert

+ (void)showAlert:(NSString*)strTitle :(NSString*)msg{
	
	UIAlertView *alert = [[UIAlertView alloc]initWithTitle:strTitle message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show]; 
	
}

+ (void)showAlertHUD:(NSString *)text{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ( !appDelgObj.HUD.superview ){
        appDelgObj.HUD = [MBProgressHUD showHUDAddedTo:appDelgObj.window animated:YES];

        appDelgObj.HUD.mode = MBProgressHUDModeText;
        appDelgObj.HUD.labelText = text;
        appDelgObj.HUD.margin = 10.f;
        appDelgObj.HUD.yOffset = 150.f;
        appDelgObj.HUD.removeFromSuperViewOnHide = YES;
        [appDelgObj.HUD hide:YES afterDelay:2];
        
    }

}

+(void)showMessage:(NSString *)msg withColor:(kMSGCOLOR)color showOnTOP:(BOOL)onTop{
    
    AppDelegate *appDelgObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:appDelgObj.window withMode:MBProgressHUDModeText animated:YES];
    
    //showHUDAddedTo:self.navigationController.view animated:YES
	
	// Configure for text only and offset down
    switch (color) {
        case kRED:
            hud.color=MSG_COLOR_RED;
            break;
        case kGREEN:
            hud.color=MSG_COLOR_GREEN;
            break;
        case kYELLOW:
            hud.color=MSG_COLOR_YELLOW;
            break;
            
        default:
            break;
    }
	
	hud.labelText = msg;
	hud.margin = 10.f;
	hud.yOffset = onTop?-150.f:150.0f;
	hud.removeFromSuperViewOnHide = YES;
	
	[hud hide:YES afterDelay:2];
}


@end
