//
//  HUDCommon.h
//  DuckFace
//
//  Created by Team Teknowledge on 2/12/13.
//  Copyright (c) 2013 Teknowledge Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HUDCommon : NSObject

+ (void)showHUD;
+ (void)showHUD:(NSString*)strTitle;
+ (void)showHUDWithTitle:(NSString *)title withSubTitle:(NSString *)subTitle;
+ (void)showHUDWithColor:(UIColor *)HUDcolor;
+ (void)showHUDinView:(UIView *)view withTitle:(NSString*)strTitle;
+ (void)showAlertHUD:(NSString *)text;
+ (void)showAlert:(NSString*)strTitle :(NSString*)msg;
+ (void)showHUDWithPercentage:(double)percentage WithText:(NSString *)text;
+ (void)showMessage:(NSString *)msg withColor:(kMSGCOLOR)color showOnTOP:(BOOL)onTop;
+ (void)killHUD;


@end
