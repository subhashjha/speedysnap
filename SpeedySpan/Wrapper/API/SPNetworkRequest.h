//
//  SPNetworkRequest.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPNetworkRequest : NSObject

+ (void)loginWithFBDeatiilsWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)createCircleWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)getFriendListWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)getCircleListWithID:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)uploadImageForCircleWithParameter:(NSMutableDictionary *)mDicParameter withImageData:(NSData *)imageData completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)deleteFromCircleUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)sendInvitation:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)AddfriendToCircle:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)deleteCircleUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
+ (void)registerForPushWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock;
@end
