//
//  SPNetworkRequest.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPNetworkRequest.h"
#import "SPFetchAPIOperator.h"

@implementation SPNetworkRequest

+ (void)loginWithFBDeatiilsWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
}
+ (void)registerForPushWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj registerForPushFile:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
}
+ (void)createCircleWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
    
}
+ (void)getFriendListWithUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];

}
+ (void)getCircleListWithID:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
}
+ (void)uploadImageForCircleWithParameter:(NSMutableDictionary *)mDicParameter withImageData:(NSData *)imageData completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj uploadImage:imageData withParameter:mDicParameter withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
}
+ (void)deleteFromCircleUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
    
}
+ (void)sendInvitation:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
    
}
+ (void)AddfriendToCircle:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
    
}
+ (void)deleteCircleUrl:(NSString *)urlString withParameter:(NSMutableDictionary *)parameter completionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    SPFetchAPIOperator *apiObj=[[SPFetchAPIOperator alloc]init];
    [apiObj getValueFromRequestURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]  withParameter:parameter  withCompletionHandler:^(NSString *responseString, NSError *error) {
        
        completionBlock(responseString,error);
        
    }];
    
}
@end
