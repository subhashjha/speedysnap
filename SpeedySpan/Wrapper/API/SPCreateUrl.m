//
//  SPCreateUrl.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPCreateUrl.h"

@implementation SPCreateUrl


-(NSString *)CreateUrlWithDetails:(NSDictionary *)dicValue withType:(kAPITYPE)type
{
      AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *url=@"";
    self.apiType=type;
    switch (self.apiType) {
            //Login Url
            case kGETLOGIN:
               url=[NSString stringWithFormat:@"%@authenticate?username=%@&email=%@&gender=%@&password=%@&socialid=%@&userprofileimage=%@",BASE_URL,[dicValue valueForKey:@"username"],[dicValue valueForKey:@"email"],[dicValue valueForKey:@"gender"],[dicValue valueForKey:@"password"],[dicValue valueForKey:@"socialid"],[dicValue valueForKey:@"userprofileimage"]];
             break;
            //Registration url
        case kREGISTER:
            url=[NSString stringWithFormat:@"%@registerUser?username=%@&email=%@&gender=%@&password=%@&socialid=%@&userprofileimage=%@",BASE_URL,[dicValue valueForKey:@"username"],[dicValue valueForKey:@"email"],[dicValue valueForKey:@"gender"],[dicValue valueForKey:@"password"],[dicValue valueForKey:@"socialid"],[dicValue valueForKey:@"userprofileimage"]];
            break;
        case kCREATECIRCLE:
            url=[NSString stringWithFormat:@"%@createCircle?admin=%@&circlename=%@",BASE_URL,[dicValue valueForKey:@"admin"],[dicValue valueForKey:@"circlename"]];
            break;
        case kFETCHFRIENDLIST:
            url=[NSString stringWithFormat:@"%@checkUserFriendCircle?userid=%@&socialid=%@&accesstoken=%@",BASE_URL,[dicValue valueForKey:@"userid"],[dicValue valueForKey:@"socialid"],[dicValue valueForKey:@"accesstoken"]];
            break;
        case KFETCHCIRCLEWITHID:
            url=[NSString stringWithFormat:@"%@fetchCircleByID?userid=%@&circleid=%@",BASE_URL,[dicValue valueForKey:@"userid"],[dicValue valueForKey:@"circleid"]];
            break;
        case kUPLOADIMAGE:
            url=[NSString stringWithFormat:@"%@uploadImageToCircle?userid=%@&circleid=%@&latitude=%f&longitude=%f",BASE_URL,[dicValue valueForKey:@"userid"],[dicValue valueForKey:@"circleid"],appDelegate.userLocation.latitude,appDelegate.userLocation.longitude];
            break;
        case KDELETEFROMCIRCLE:
            url=[NSString stringWithFormat:@"%@removeFromCircle?friendid=%@&circleid=%@",BASE_URL,[dicValue valueForKey:@"friendid"],[dicValue valueForKey:@"circleid"]];
            break;
        case KFETCHALLCIRCLE:
            url=[NSString stringWithFormat:@"%@fetchAllCircles?userid=%@",BASE_URL,[dicValue valueForKey:@"userid"]];
            break;
        case kSENDINVITATION:
            url=[NSString stringWithFormat:@"%@sendInvitation?userid=%@&friendemail=%@",BASE_URL,[dicValue valueForKey:@"userid"],[dicValue valueForKey:@"email"]];
            break;
        case kADDFriend:
            url=[NSString stringWithFormat:@"%@insertFriendCircle?userid=%@&circleid=%@&friendlist=%@",BASE_URL,[dicValue valueForKey:@"userid"],[dicValue valueForKey:@"circleid"],[dicValue valueForKey:@"socialid"]];
            break;
        case kDELETECIRCLE:
            url=[NSString stringWithFormat:@"%@deleteCircle?admin=%@&circleid=%@",BASE_URL,[dicValue valueForKey:@"userid"],[dicValue valueForKey:@"circleid"]];
            break;
            
        default:
            break;
   
    }
    return url;
}
@end

