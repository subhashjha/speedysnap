//
//  SPCreateUrl.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    kGETLOGIN = 1,
    kREGISTER = 2,
    kCREATECIRCLE=3,
    kFETCHFRIENDLIST=4,
    KFETCHCIRCLEWITHID=5,
    kUPLOADIMAGE=6,
    KDELETEFROMCIRCLE=7,
    KFETCHALLCIRCLE=8,
    kSENDINVITATION=9,
    kADDFriend=10,
    kDELETECIRCLE=11,
    
    
}kAPITYPE;
@interface SPCreateUrl : NSObject
@property(assign)kAPITYPE apiType;
-(NSString *)CreateUrlWithDetails:(NSDictionary *)dicValue withType:(kAPITYPE)type;
@end
