//
//  SPFetchAPIOperator.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import "SPFetchAPIOperator.h"
#import "AFNetworking.h"
#import "Reachability.h"
#import "VendorID.h"
@implementation SPFetchAPIOperator
-(void)getValueFromRequestURL:(NSURL *)reqURL withParameter:(NSDictionary *)dicParameter  withCompletionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    //Assign completion Block
    _completionBlock=completionBlock;
   
    //Start connection request
    // Create the request.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:reqURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    //request setde
    [request setHTTPMethod:@"POST"];
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    [conn start];
    
}
-(void)registerForPushFile:(NSURL *)reqURL withParameter:(NSDictionary *)dicParameter  withCompletionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    //Assign completion Block
    _completionBlock=completionBlock;

    UIDevice *dev = [UIDevice currentDevice];
	NSString *deviceUuid = VENDOR_UUID;
	NSString *deviceSystemVersion = dev.systemVersion;
	NSData *tokenData = [[NSUserDefaults standardUserDefaults]objectForKey:@"token"];
	// Prepare the Device Token for Registration (remove spaces and < >)
	NSString *deviceToken = [[[[tokenData description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSString *url = [NSString stringWithFormat:@"%@registerDevice",BASE_URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
      [request setHTTPMethod:@"POST"];
    NSString *string = [NSString stringWithFormat:@"userid=%@&os=%@&device_id=%@&is_iphone=%@&device_token=%@",GETVALUE(@"userid"),deviceSystemVersion,deviceUuid,@"1",deviceToken];
   [request setHTTPBody:[string dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    [conn start];
    
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
    
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
    
    
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
    //completion block implement to finish up response utilization
    NSString *strResponse = [[NSString alloc]initWithData:_responseData encoding:NSUTF8StringEncoding];
     _responseData=nil;
    _completionBlock(strResponse,nil);
    
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
   
     NSString *strResponse = [[NSString alloc]initWithData:_responseData encoding:NSUTF8StringEncoding];
     _responseData=nil;
    _completionBlock(strResponse,error);
}
-(void)uploadImage:(NSData *)imageData withParameter:(NSDictionary *)dicParameter  withCompletionHandler:(RequestConnectionStringResponseBlock)completionBlock
{
    _completionBlock=completionBlock;

    Reachability *reach = [Reachability reachabilityForInternetConnection];
    if([reach isReachable])
    {
      
        NSURL *url = [NSURL URLWithString:BASE_URL1];
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"api/uploadImageToCircle" parameters:dicParameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"attachment.jpg" mimeType:@"image/jpg"];
        }];
        [request setTimeoutInterval:60];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *dataString = [[NSString alloc]
                                    initWithData:responseObject
                                    encoding:NSASCIIStringEncoding];
           _completionBlock(dataString,nil);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error = %@",error.debugDescription);
    
             _completionBlock(nil,error);
           
        }];
        [operation start];
    }
    else
    {
        [HUDCommon showMessage:NO_INTERNET_CONNECTION withColor:kRED showOnTOP:YES];
        
    }
    
    
}

@end
