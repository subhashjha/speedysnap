//
//  NSObject+NetworkCommon.m
//  SoundSurfer
//
//  Created by Team Teknowledge on 2/4/14.
//  Copyright (c) 2014 Teknowledge Software. All rights reserved.
//

#import "NSObject+NetworkCommon.h"

@implementation NSObject (NetworkCommon)

//parse response data using json serialization
+(NSJSONSerialization *)getParseJsonResponse:(NSMutableData *)responseData{
    
    NSError *jsonError=nil;
    NSJSONSerialization *jsonResponse=nil;
    if(responseData!=nil)
        jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
    
    return jsonResponse;
}



@end
