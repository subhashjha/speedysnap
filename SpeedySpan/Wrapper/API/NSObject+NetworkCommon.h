//
//  NSObject+NetworkCommon.h
//
//
//  Created by Team Teknowledge on 21/1/13.
//  Copyright (c) 2013 Teknowledge Software. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (NetworkCommon)

+(NSJSONSerialization *)getParseJsonResponse:(NSMutableData *)responseData;



@end
