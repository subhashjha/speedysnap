//
//  SPFetchAPIOperator.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "AFNetworking.h"

@interface SPFetchAPIOperator : NSObject
/** Response Data after parsing **/
@property (nonatomic, strong) NSMutableData *responseData;
/** Completion Block instance **/
@property (nonatomic, strong) RequestConnectionStringResponseBlock completionBlock;
//For regular network request


-(void)getValueFromRequestURL:(NSURL *)reqURL withParameter:(NSDictionary *)dicParameter  withCompletionHandler:(RequestConnectionStringResponseBlock)completionBlock;
-(void)uploadImage:(NSData *)imageData withParameter:(NSDictionary *)dicParameter  withCompletionHandler:(RequestConnectionStringResponseBlock)completionBlock;
-(void)registerForPushFile:(NSURL *)reqURL withParameter:(NSDictionary *)dicParameter  withCompletionHandler:(RequestConnectionStringResponseBlock)completionBlock;
@end
