//
//  SPNetworkConstant.h
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 27/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum {
    
    NetworkRequestTypeGET                     =1,
    NetworkRequestTypePOST                    =2,
    
} NetworkRequestType;


typedef enum {
    NetworkURLParameterEncoding                    =1,
    NetworkJSONParameterEncoding                   =2,
    NetworkPropertyListParameterEncoding           =3,
    NetworkParameterEncodingNone                   =4,
    NetworkUploadWithXMLParameterEncoding          =5,
} NetworkParameterEncoding;

static inline NSString * NetworkOperationRequestType(NetworkRequestType type) {
    switch (type) {
        case NetworkRequestTypeGET:
            return @"GET";
        case NetworkRequestTypePOST:
            return @"POST";
        default:
            return @"type";
    }
}typedef signed short AFOperationState;


typedef void (^RequestConnectionStringResponseBlock) (NSString *requestString, NSError *error);

typedef void (^RequestConnectionDictionaryResponseBlock) (NSDictionary *dicRequest, NSError *error);

typedef void (^RequestConnectionArrayResponseBlock)(NSMutableArray *mutArrRequest, NSError *error);

typedef void (^RequestdidFinishCompletionBlock) (NSMutableData *responseData, NSError *error);


