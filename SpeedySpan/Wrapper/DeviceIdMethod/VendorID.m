//
//  VendorID.m
//  Storytime
//
//  Created by Ved Prakash on 6/11/13.
//
//

#import "VendorID.h"
#import "KeychainItemWrapper.h"

@implementation VendorID


+(NSString *)getUUID {
    //Initialize the keychain
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:[[NSBundle mainBundle] bundleIdentifier] accessGroup:nil];
    
    NSString *uuid = [NSString stringWithString:[wrapper objectForKey:((__bridge id)kSecAttrAccount)]];
    
    /*NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     NSString *uuid = [userDefaults objectForKey:@"UUID"];*/
    if ((uuid == nil) || (uuid.length == 0)) {
        CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
        
        // Get the string representation of CFUUID object.
        uuid = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
        CFRelease(uuidObject);
        
        /*NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
         [userDefaults setObject:uuid forKey:@"UUID"];
         [userDefaults synchronize];*/
        
        //Save into keychain
        [wrapper setObject:uuid forKey:(__bridge id)kSecAttrAccount];
    }
    
    return uuid;
}
@end
