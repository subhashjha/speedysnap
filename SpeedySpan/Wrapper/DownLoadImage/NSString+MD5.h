//
//  NSString+MD5.h
//  Indigo
//
//  Created by Raymond To on 07/10/2013.
//  Copyright (c) 2013 Artificial Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end

