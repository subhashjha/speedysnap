#import "SPParserJsonResponse.h"
//FB details
#define FB_APP_ID @"1400177896911096"

//#define FB_API_KEY @"3269fff9ef3b6fc13255e670ebb44c4d"
//#define FB_APP_SECRET @"226433056e13ad4dab1d023732f54ef3"

//Hud alert color

#define HUD_COLOR [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]

#define MSG_COLOR_GREEN [UIColor colorWithRed:101.0/255.0 green:171.0/255.0 blue:53.0/255.0 alpha:1.0]

#define MSG_COLOR_RED [UIColor colorWithRed:189.0/255.0 green:21.0/255.0 blue:16.0/255.0 alpha:1.0]

#define MSG_COLOR_YELLOW [UIColor colorWithRed:208.0/255.0 green:180.0/255.0 blue:49.0/255.0 alpha:1.0]

//Message color

typedef enum {
    kRED            = 1,
    kGREEN          = 2,
    kYELLOW         = 3
}kMSGCOLOR;

//URL used in APP
#define BASE_URL [NSString stringWithFormat:@"http://staging.teks.co.in/speedysnap/index.php/api/"]
#define BASE_URL1 [NSString stringWithFormat:@"http://staging.teks.co.in/speedysnap/index.php/"]
#define BASE_URL2 [NSString stringWithFormat:@"http://nearvacancy.com/nearvacancy/index.php/"]
//Check if API returns NULL value
#define IsEmpty(value) (value == (id)[NSNull null] || value == nil || ([value isKindOfClass:[NSString class]] && ([value isEqualToString:@""] ||  [value isEqualToString:@"<null>"]))) ? YES : NO

#define IfNULL(original, replacement) IsNULL(original) ? replacement : original

#define IsNULL(original) original == (id)[NSNull null]
#define debugLog    0
#define SafeString(value) IfNULL(value, @"")
//if iphone 5
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


//Network error message
#define NO_INTERNET_CONNECTION @"Oops! It seems there is a network problem. Please check your network connection and try again."
//Check for device version
#define deviceIos  [UIDevice currentDevice].systemVersion
//Fonts for the app
#define OPENSANS_EXTRABOLSIT(SIZE) [UIFont fontWithName:@"OpenSans-ExtraboldItalic" size:SIZE]
#define OPENSANS_SEMIBOLSIT(SIZE) [UIFont fontWithName:@"OpenSans-SemiboldItalic" size:SIZE]
#define OPENSANS_EXTRABOLD(SIZE) [UIFont fontWithName:@"OpenSans-Extrabold" size:SIZE]
#define OPENSANS_BOLDIT(SIZE) [UIFont fontWithName:@"OpenSans-BoldItalic" size:SIZE]
#define OPENSANS_IT(SIZE) [UIFont fontWithName:@"OpenSans-Italic" size:SIZE]
#define OPENSANS_SEMIBOLD(SIZE) [UIFont fontWithName:@"OpenSans-Semibold" size:SIZE]
#define OPENSANS_LIGHT(SIZE) [UIFont fontWithName:@"OpenSans-Light" size:SIZE]
#define OPENSANS(SIZE) [UIFont fontWithName:@"OpenSans" size:SIZE]
#define OPENSANS_LIGHTIT(SIZE) [UIFont fontWithName:@"OpenSansLight-Italic" size:SIZE]
#define OPENSANS_BOLD(SIZE) [UIFont fontWithName:@"OpenSans-Bold" size:SIZE]

//Update userdefault value
#define UPDATEVALUE(key,value){\
[[NSUserDefaults standardUserDefaults] setValue:value forKey:key];\
[[NSUserDefaults standardUserDefaults] synchronize];\
}
//Update userdefault value
#define UPDATEBOOLVALUE(key,value){\
[[NSUserDefaults standardUserDefaults] setBool:value forKey:key];\
[[NSUserDefaults standardUserDefaults] synchronize];\
}
//Remove userdefault value
#define REMOVEVALUE(key){\
[[NSUserDefaults standardUserDefaults] removeObjectForKey:key];\
[[NSUserDefaults standardUserDefaults] synchronize];\
}
//Get user default value from key
#define GETVALUE(key)[[NSUserDefaults standardUserDefaults] valueForKey:key]
//Get user default bool value from key
#define GETBOOLVALUE(key)[[NSUserDefaults standardUserDefaults] boolForKey:key]
