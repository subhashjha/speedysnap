//
//  main.m
//  SpeedySpan
//
//  Created by Subhash Kr. Jha on 25/02/14.
//  Copyright (c) 2014 Teks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
